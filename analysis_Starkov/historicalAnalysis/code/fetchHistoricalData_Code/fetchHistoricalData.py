# coding=utf-8
import pickle
import urllib,time,datetime

# script that gets company names from "./listNasdaq.txt" file,
# connects to yahoo finance and collects csv file from the the first X companies
# saves the data in "../historicalData/" directory
# the name of the data file saved is the name of the company

nbCompaniesToFetch = 1000

totCompanies_file = "../../data/listNasdaq/listNasdaq_tot.txt"
usedCompanies_file = "../../data/listNasdaq/listNasdq_used.txt"

historicalData_dir = "../../data/historicalData_data/"

class getHistData:

	def __init__(self):
		self.listOfComp = list()

	def readFile(self):
		f = open(totCompanies_file,"r")
		i = 0
		while i < nbCompaniesToFetch:
			string = f.readline()
			if "^" in string: continue
			self.listOfComp.append(string.rstrip().replace("/","-"))
			i += 1
		f.close()

	def getData(self):
		for name in self.listOfComp:
			url_string = "http://ichart.finance.yahoo.com/table.csv?s="+name+"&a=07&b=19&c=2010&d=06&e=31&f=2013&g=d&ignore=.csv"
			urllib.urlretrieve(url_string, historicalData_dir+name)
		pickle.dump(self.listOfComp, open(usedCompanies_file, 'wb'))


d = getHistData()
d.readFile()
d.getData()