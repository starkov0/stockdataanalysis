# coding=utf-8
import urllib,time,datetime
import matplotlib.pyplot as plt
import math
import csv
import numpy as np
import pickle
import scipy

# files and directories used in this script
analysisPlots_dir = "../../data/analysis_plots/"
analysisData_dir = "../../data/analysis_data/"
historicalData_dir = "../../data/historicalData_data/"

usedCompanies_file = "../../data/listNasdaq/listNasdq_used.txt"

# AKA best plot
bestRaise_name = '-AKA BEST RAISE-'


##################################################################################


class historicalQuotesAnalysis():

  # initiation -> creation of list of variables
  def __init__(self):
    self.symbol = ''
    self.listSymbols = []
    self.date,self.open_,self.high,self.low,self.close,self.volume,self.adjClose = ([] for _ in range(7))
    self.raisePerWeek = [] # vector of raise
    self.bestRaisePerWeek = []




################################################################################## TEXT-DATA FUNCTIONS




  # allows to iterate analysis for each Company
  def analyseHistoricalData(self):
    self.readListOfCompanies()
    for symbol in  self.listSymbols:
      self.readCompanyData(symbol)
      self.getPriceRise()
      self.getBiggestPriceRaise()
      self.plotRaiseAndHistogram(self.raisePerWeek, self.symbol) # plots raise per company
      self.writeRaise(self.raisePerWeek, self.symbol) # writing raise per company in a file
    self.plotRaiseAndHistogram(self.bestRaisePerWeek, bestRaise_name) # plots best raise
    self.writeRaise(self.bestRaisePerWeek, bestRaise_name) # write best raise in a file




  # read the file containing the list of all the companies we need
  def readListOfCompanies(self):
    self.listSymbols = pickle.load(open(usedCompanies_file, 'rb'))




  # reads a the company data in a CSV type file
  def readCompanyData(self,filename):
    self.symbol = filename
    self.date,self.open_,self.high,self.low,self.close,self.volume,self.adjClose = ([] for _ in range(7))
    self.raisePerWeek = []
    f = open(historicalData_dir+filename,"r")
    f.next()
    for line in f:
      [date,open_,high,low,close,volume,adjClose] = line.rstrip().split(',')
      self.append(date,open_,high,low,close,volume,adjClose)




  # adds number to self vectors
  def append(self,date,open_,high,low,close,volume,adjClose):
    self.date.append(datetime.datetime.strptime(date, '%Y-%m-%d').date())
    self.open_.append(float(open_))
    self.high.append(float(high))
    self.low.append(float(low))
    self.close.append(float(close))
    self.volume.append(float(volume))
    self.adjClose.append(float(adjClose))


  

  def plotRaiseAndHistogram(self,data,name):
    maxRaise = np.max(data)
    # n, bins, patches = plt.hist(data, 50, normed=1, facecolor='green', alpha=0.5)
    # y = mlab.normpdf(bins, mu, sigma)
    # hist, bins,  = plt.hist(data, 50)
    mean = np.mean(data)
    median = np.median(data)
    variance = np.var(data)

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(data, color = 'r')
    ax1.set_xlabel("Weeks")
    ax1.set_ylabel("Raise (%)")
    ax2 = fig.add_subplot(212)
    ax2.hist(data, 50)
    #np.linspace(0, max(hist[0]), len(hist[0]) ), hist[0], color = 'b')
    ax2.set_xlabel("Raise (%)")
    ax2.set_ylabel("Frequency")

    plt.suptitle("Stocks Raise Per Week : "+name+"\nmean = {0:.2f}, median = {1:.2f}, variance = {2:.2f}".format(mean, median, variance))
    plt.savefig(analysisPlots_dir + name + ".png")




  # writes data in a CSV file
  def writeRaise(self,data,name):
    resultFile = open(analysisData_dir+name+".csv",'wb')
    wr = csv.writer(resultFile, dialect='excel')
    for item in data:
      wr.writerow([item,])




################################################################################## NUMERICAL-DATA FUNCTIONS




  # get pourcentage of high - low changement
  # this function groupes days of one week by comparing variables day1 and day2
  # if day1 if smaller than day2, then day2 is still in the same week day as day1
  # otherwise a new week starts                                                                  
  def getPriceRise(self):

    # adding the first date
    day1 = int(self.date[0].strftime('%u'))
    tmp_low = list()
    tmp_high = list()
    tmp_low.append(self.low[0])
    tmp_high.append(self.high[0])

    for i in range(1, len(self.date)):
      day2 = int(self.date[i].strftime('%u'))

      if day1 > day2: # check if new week starts

        priceRaise = self.getPriceRaisePerWeek(tmp_low,tmp_high)
        self.raisePerWeek.append(priceRaise)

        day1 = day2 # change day1 to day1
        tmp_low = list()
        tmp_high = list()
        tmp_low.append(self.low[i])
        tmp_high.append(self.high[i])

      else:

        day1 = day2
        tmp_low.append(self.low[i])
        tmp_high.append(self.high[i])


  # # calculates the biggest raise per week for a company                                                             
  def getPriceRaisePerWeek(self,low,high):
    priceRaise = 0
    for i in range(len(low)):
      for j in range(i,len(low)):
        tmp = (high[j] - low[i]) / low[i]
        if tmp > priceRaise:
          priceRaise = tmp
    return tmp * 100

  def getBiggestPriceRaise(self):
    if len(self.bestRaisePerWeek) == 0:
      self.bestRaisePerWeek = self.raisePerWeek
    else:
      for i in range(len(self.raisePerWeek)):
        if self.bestRaisePerWeek[i] < self.raisePerWeek[i]:
          self.bestRaisePerWeek[i] = self.raisePerWeek[i]

##################################################################################


anal = historicalQuotesAnalysis()
anal.readListOfCompanies()
anal.analyseHistoricalData()
