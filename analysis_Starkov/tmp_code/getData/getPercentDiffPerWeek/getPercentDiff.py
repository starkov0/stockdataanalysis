# coding=utf-8
import urllib,time,datetime
import matplotlib.pyplot as plt
import math
import csv
import numpy as np


##################################################################################


class Quote(object):
  
  DATE_FMT = '%Y-%m-%d'
  TIME_FMT = '%H:%M:%S'
  
  def __init__(self):
    self.symbol = ''
    self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
    self.header = []



                                                                        # TEXTDATA FUNCTIONS


  # creates vectors of numbers
  def append(self,dt,open_,high,low,close,volume):
    self.date.append(dt.date())
    self.time.append(dt.time())
    self.open_.append(float(open_))
    self.high.append(float(high))
    self.low.append(float(low))
    self.close.append(float(close))
    self.volume.append(int(volume))

  # adds a header to the whole data
  def append_header(self,header):
    self.header.append(header)
      
  # computes data from list format to CSV format
  def to_csv(self):
    return ''.join(["{0},{1},{2},{3:.2f},{4:.2f},{5:.2f},{6:.2f},{7}\n".format(self.symbol,
              self.date[bar].strftime('%Y-%m-%d'),self.time[bar].strftime('%H:%M:%S'),
              self.open_[bar],self.high[bar],self.low[bar],self.close[bar],self.volume[bar]) 
              for bar in xrange(len(self.close))])

  # writes data in a CSV file
  def write_csv(self,filename):
    f = open(filename,'w')
    f.write('#############################################\n')
    for head in self.header:
      f.write(head)
    f.write('#############################################\n')
    f.write('\n')
    f.close()
    with open(filename,'a') as f:
      f.write(self.to_csv())
        
  # reads a csv file
  def read_csv(self,filename):
    self.symbol = ''
    self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
    for line in open(filename,'r'):
      symbol,ds,ts,open_,high,low,close,volume = line.rstrip().split(',')
      self.symbol = symbol
      dt = datetime.datetime.strptime(ds+' '+ts,self.DATE_FMT+' '+self.TIME_FMT)
      self.append(dt,open_,high,low,close,volume)
    return True

  # to_string function
  def __repr__(self):
    return self.to_csv()





                                                                        # NUMERICALDATA FUNCTIONS

  # get pourcentage of high - low changement
  # this function groupes days of one week by comparing variables day1 and day2
  # if day1 if smaller than day2, then day2 is still in the same week day as day1
  # otherwise a new week starts                                                                  
  def get_percent_of_high_low_changments(self):
    # lists to be returned by the function
    dates = list()
    nb_days_per_week = list()
    percentage_per_week = list()

    # adding the first date
    day1 = int(self.date[0].strftime('%u'))
    date = self.date[0].strftime('%A-%d-%m-%Y')
    tmp_low = list()
    tmp_high = list()
    tmp_low.append(self.low[0])
    tmp_high.append(self.high[0])
    nb_days = 1

    for i in range(1, len(self.date)):
      day2 = int(self.date[i].strftime('%u'))

      if day1 > day2:

        dates.append(date)
        nb_days_per_week.append(nb_days)
        percentage_per_week.append(self.get_percentage_of_high_low_chagments_per_week(tmp_low,tmp_high))

        date = self.date[i].strftime('%A-%d-%m-%Y')
        day1 = day2
        tmp_low = list()
        tmp_high = list()
        tmp_low.append(self.low[i])
        tmp_high.append(self.high[i])
        nb_days = 1

      else:

        day1 = day2
        tmp_low.append(self.low[i])
        tmp_high.append(self.high[i])
        nb_days += 1
    return dates, nb_days_per_week, percentage_per_week


  # # get pourcentage of high - low changement per week                                                                  
  def get_percentage_of_high_low_chagments_per_week(self,low,high):
    return(max(high) - min(low)) * 100 / min(low)


##################################################################################


class GoogleIntradayQuote(Quote):
  ''' Intraday quotes from Google. Specify interval seconds and number of days '''
  def __init__(self,symbol,interval_seconds=300,num_days=5):
    super(GoogleIntradayQuote,self).__init__()
    self.symbol = symbol.upper()
    url_string = "http://www.google.com/finance/getprices?q={0}".format(self.symbol)
    url_string += "&i={0}&p={1}d&f=d,o,h,l,c,v".format(interval_seconds,num_days)
    csv = urllib.urlopen(url_string).readlines()
    # print header
    for i in range(5):
      self.append_header(csv[i]) 
    # print data
    for bar in xrange(7,len(csv)):
      if csv[bar].count(',')!=5: continue
      offset,close,high,low,open_,volume = csv[bar].split(',')
      if offset[0]=='a':
        day = float(offset[1:])
        offset = 0
      else:
        offset = float(offset)
      open_,high,low,close = [float(x) for x in [open_,high,low,close]]
      dt = datetime.datetime.fromtimestamp(day+(interval_seconds*offset))
      self.append(dt,open_,high,low,close,volume)


  def data_nb_days_percentage_to_csv_helper(self, date, days, percentage):
    data = list()
    for i in range(len(date)):
      data.append([date[i], days[i], percentage[i]])
    return data

  def data_to_csv_header(self,name):
    b = open(name, 'wb')
    b.write('#############################################\n')
    b.write('DATE, NB DAYS PER WEEK, PERCENTAGE OF LINEAR CHANGE IN DURING THE WEEK\n')
    b.write('#############################################\n\n')
    b.close()


  def data_to_csv(self, name, data, company, stat):
    b = open(name, 'a')
    b.write("company name : " + company + "\n")
    b.write("median : " + str(stat[0]) + ", mean : " + str(stat[1]) + ", variance : " + str(stat[2]) + "\n")
    a = csv.writer(b)
    a.writerows(data)
    b.write("\n")
    b.close()

##################################################################################
name = "PercentDifferencePerWeek.txt"
tot_comapnies_names = ["FB","GOOG","BIDU","ETFC","AMZN","ZNGA","AAPL","FAST"]
##################################################################################

##################################################################################
# opening a text file and writting a header
b = open(name, 'wb')
b.write('#############################################\n')
b.write('DATE, NB DAYS PER WEEK, PERCENTAGE OF LINEAR CHANGE IN DURING THE WEEK\n')
b.write('#############################################\n\n')
b.close()
##################################################################################

##################################################################################
for company_symbole in tot_comapnies_names:
  # company_symbole = 'FB'
  interval_sec = 3600 # number of seconds in one day
  nb_days_market_data = 1000

  # connecting to goolge
  googData = GoogleIntradayQuote(company_symbole,interval_sec,nb_days_market_data)

  print len(googData.date)

  # getting first day of the week, nb days per week and percentage of linear difference between high and low during the week
  fisrt_date_of_week, nb_days_per_week, percent_diff_per_week = googData.get_percent_of_high_low_changments()

  median = np.median(percent_diff_per_week)
  mean = np.mean(percent_diff_per_week)
  var = np.var(percent_diff_per_week)

  # putting data in the right way
  data = googData.data_nb_days_percentage_to_csv_helper(fisrt_date_of_week, nb_days_per_week, percent_diff_per_week)
  # printing data
  googData.data_to_csv(name ,data, company_symbole, [median, mean, var])
##################################################################################

##################################################################################
# plotting the graphs
  fig = plt.figure()
  ax2 = fig.add_subplot(111)
  ax2.plot(percent_diff_per_week, color = 'r')
  plt.xlabel("Week")
  plt.ylabel("Percentage Difference")
  plt.suptitle("Linear Analysis - Percentage Difference - Per Week\nmean : "+str(mean)+", median : "+str(median)+", variance : "+str(var))
  plt.savefig(company_symbole + ".png")
