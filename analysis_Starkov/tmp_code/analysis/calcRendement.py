import csv
from numpy import mean, max, min, array

def readCSV(name):
    data = []
    resultFile = open(name,'rb')
    rd = csv.reader(resultFile, dialect='excel')
    for row in rd:
        data.append(row[1])
    return data

data = array(readCSV('test1.csv'), dtype=float)

print data
print 'mean : ' + str(mean(data))
print 'min : ' + str(min(data))
print 'max : ' + str(max(data))
print sum(data) - len(data)*10000