# coding=utf-8
from DecisionTree.DT_code.MinMaxPrice.DT import DT
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType
from numpy import corrcoef, arange
import matplotlib.pyplot as plt

##################################################################################
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)
##################################################################################

class corrCoefAttributs(DT):

    def __init__(self,varObj,dataObj):
        super(corrCoefAttributs, self).__init__(varObj,dataObj)
        pickle(MethodType, _pickle_method, _unpickle_method)

##################################################################################
    def setAttributsAndClassesCC(self,attributs):
        self.attributs = attributs

    def computeCorrCoef(self):
        R = corrcoef(self.attributs)
        plt.imshow(R, interpolation='bilinear')
        plt.colorbar()
        plt.yticks(arange(0.5,52.5),range(0,52))
        plt.xticks(arange(0.5,52.5),range(0,52))
        plt.savefig(self.varObj.corrcoef_plot_dir+self.dataObj.symbol+".png")
        self.writeCSV1(R, self.varObj.corrcoef_data_Attributs_dir+self.dataObj.symbol+"Attributs.csv")
        
    def fillAttributsAndClassesCC(self):
        h = 0
        nbTrainWeeks = self.varObj.nbTrainWeeks
        for companyNumber in xrange(self.varObj.trainCompanies_Start,self.varObj.trainCompanies_End):
            self.loadCompanyData(companyNumber)
            for week in xrange(nbTrainWeeks):
                h = self.fillAttributsAndClassesCCHelp(week,h)
        self.attributs = self.attributs.transpose()

    def fillAttributsAndClassesCCHelp(self,week,h):
        self.dataObj.week = week
        futur_time = self.varObj.futur_time
        startAnalysisValue = self.varObj.startAnalysisValue
        length = len(self.dataObj.weekData[week][0])-futur_time
        time = range(startAnalysisValue,length)

        pool1 = Pool()
        async_results1 = [ pool1.apply_async(self.getInput, (i,)) for i in time ]
        map(ApplyResult.wait, async_results1)
        input_=[r.get() for r in async_results1]
        self.attributs[h:h+len(time),:] = input_

        h += len(time)
        return h
