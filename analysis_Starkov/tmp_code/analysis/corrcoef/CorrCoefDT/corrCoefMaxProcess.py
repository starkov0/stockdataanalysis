# coding=utf-8
from DecisionTree.DT_code.CorrCoef.corrCoefMax import corrCoefMax
from multiprocessing import Process

##################################################################################

class corrCoefMaxProcess(corrCoefMax,Process):

    def __init__(self,varObj,dataObj):
        super(corrCoefMaxProcess, self).__init__(varObj,dataObj)
        self.daemon = True

    def run(self):
        self.fillAttributsAndClassesCC()
        self.computeCorrCoef()
        print "finished corrCoefMax computation"
