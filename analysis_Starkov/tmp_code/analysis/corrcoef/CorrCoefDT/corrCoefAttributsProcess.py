# coding=utf-8
from DecisionTree.DT_code.CorrCoef.corrCoefAttributs import corrCoefAttributs
from multiprocessing import Process

##################################################################################

class corrCoefAttributsProcess(corrCoefAttributs,Process):

    def __init__(self,varObj,dataObj):
        super(corrCoefAttributsProcess, self).__init__(varObj,dataObj)
        self.daemon = True

    def run(self):
        self.fillAttributsAndClassesCC()
        self.computeCorrCoef()
        print "finished corrCoefAttributs computation"
