import csv
import os, glob
import numpy as np
import matplotlib.pyplot as plt
from Variables_Data.Variables.Variables import Variables



class analysisCC(object):
    
    def __init__(self):
        super(analysisCC, self).__init__()
        self.varObj = Variables()
        self.maxCC = []
        self.minCC = []
        self.priceCC = []

##################################################################################
    def readCSV1(self,name):
        data = []
        resultFile = open(name,'rb')
        rd = csv.reader(resultFile, dialect='excel')
        for row in rd:
            data.append(row[0])
        return data
    
    def writeCSV1(self,data,name):
        resultFile = open(name,'wb')
        wr = csv.writer(resultFile, dialect='excel')
        for i in xrange(len(data)):
            wr.writerow([data[i],])
        resultFile.close()
        
    def plotMeanCC(self):
        main_dir = "../../../" 
        data_dir = self.varObj.corrcoef_analysis_dir
        ll = len(self.meanMaxCC)
        x_axe = range(len(self.meanMinCC))
        xMax_axe = range(len(self.meanMaxCC))
        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.set_ylabel("Max")
        plt.xticks(np.arange(0,ll),range(0,ll))
        ax2 = fig.add_subplot(312)
        ax2.set_ylabel("Min")
        plt.xticks(np.arange(0,ll),range(0,ll))
        ax3 = fig.add_subplot(313)
        ax3.set_ylabel("Price")
        plt.xticks(np.arange(0,ll),range(0,ll))
        ax1.plot(xMax_axe,abs(self.meanMaxCC))
        ax2.plot(x_axe,abs(self.meanMinCC))
        ax3.plot(x_axe,abs(self.meanPriceCC))
        plt.savefig(main_dir+data_dir+"meanCC.png")
        plt.show()
    
##################################################################################
    def getMaxCC(self):
        current_dir = os.getcwd()
        main_dir = "../../../" 
        data_dir = self.varObj.corrcoef_data_Max_dir
        os.chdir(main_dir+data_dir)
        for file_ in glob.glob("*.csv"):
            self.maxCC.append(self.readCSV1(file_))
        os.chdir(current_dir)
        
    def getMinCC(self):
        current_dir = os.getcwd()
        main_dir = "../../../" 
        data_dir = self.varObj.corrcoef_data_Min_dir
        os.chdir(main_dir+data_dir)
        for file_ in glob.glob("*.csv"):
            self.minCC.append(self.readCSV1(file_))
        os.chdir(current_dir)
        
    def getPriceCC(self):
        current_dir = os.getcwd()
        main_dir = "../../../" 
        data_dir = self.varObj.corrcoef_data_Price_dir
        os.chdir(main_dir+data_dir)
        for file_ in glob.glob("*.csv"):
            self.priceCC.append(self.readCSV1(file_))
        os.chdir(current_dir)
        
    def getCC(self):
        self.getMaxCC()
        self.getMinCC()
        self.getPriceCC()
    
##################################################################################
    def computeMeanCC(self,totCC):
        nbVectors = len(totCC)
        nbVariables = len(totCC[0])
        meanCC = np.zeros([1,nbVariables])[0]
        for vector in totCC:
            for i in xrange(nbVariables):
                meanCC[i] += float(vector[i])
        meanCC = meanCC/nbVectors
        return meanCC
    
    def getMeanCC(self):
        self.meanMaxCC = self.computeMeanCC(self.maxCC)
        self.meanMinCC = self.computeMeanCC(self.minCC)
        self.meanPriceCC = self.computeMeanCC(self.priceCC)
        
    def saveMeanCC(self):
        main_dir = "../../../" 
        data_dir = self.varObj.corrcoef_analysis_dir
        self.writeCSV1(self.meanMaxCC, main_dir+data_dir+"meanMaxCC.csv")
        self.writeCSV1(self.meanMinCC, main_dir+data_dir+"meanMinCC.csv")
        self.writeCSV1(self.meanPriceCC, main_dir+data_dir+"meanPriceCC.csv")
            
##################################################################################            
anal = analysisCC()
anal.getCC()
anal.getMeanCC()
anal.plotMeanCC()
anal.saveMeanCC()
print "Done"

