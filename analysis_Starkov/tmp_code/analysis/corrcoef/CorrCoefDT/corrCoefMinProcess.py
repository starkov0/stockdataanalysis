# coding=utf-8
from DecisionTree.DT_code.CorrCoef.corrCoefMin import corrCoefMin
from multiprocessing import Process

##################################################################################

class corrCoefMinProcess(corrCoefMin,Process):

    def __init__(self,varObj,dataObj):
        super(corrCoefMinProcess, self).__init__(varObj,dataObj)
        self.daemon = True

    def run(self):
        self.fillAttributsAndClassesCC()
        self.computeCorrCoef()
        print "finished corrCoefMin computation"
