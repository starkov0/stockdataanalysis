# coding=utf-8
from DecisionTree.DT_code.CorrCoef.corrCoefPrice import corrCoefPrice
from multiprocessing import Process

##################################################################################

class corrCoefPriceProcess(corrCoefPrice,Process):

    def __init__(self,varObj,dataObj):
        super(corrCoefPriceProcess, self).__init__(varObj,dataObj)
        self.daemon = True

    def run(self):
        self.fillAttributsAndClassesCC()
        self.computeCorrCoef()
        print "finished corrCoefPrice computation"
