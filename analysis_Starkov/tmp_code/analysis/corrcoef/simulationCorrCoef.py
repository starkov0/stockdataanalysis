# coding=utf-8
import numpy as np
from Simulation.Init.simulationInit import simulationInit

from DecisionTree.DT_code.CorrCoef.corrCoefAttributsProcess import corrCoefAttributsProcess
from DecisionTree.DT_code.CorrCoef.corrCoefMaxProcess import corrCoefMaxProcess
from DecisionTree.DT_code.CorrCoef.corrCoefMinProcess import corrCoefMinProcess
from DecisionTree.DT_code.CorrCoef.corrCoefPriceProcess import corrCoefPriceProcess


class simulationCorrCoef(simulationInit):

    def __init__(self):
        super(simulationCorrCoef, self).__init__()

##################################################################################
    def createCorrcoefAttributs(self):
        self.corrCoefAttributs.append(corrCoefAttributsProcess(self.varObj,self.dataObj))
        
    def createCorrcoefMax(self):
        self.corrCoefMinMaxPriceList.append(corrCoefMaxProcess(self.varObj,self.dataObj))
        
    def createCorrcoefMin(self):
        self.corrCoefMinMaxPriceList.append(corrCoefMinProcess(self.varObj,self.dataObj))
        
    def createCorrcoefPrice(self):
        self.corrCoefMinMaxPriceList.append(corrCoefPriceProcess(self.varObj,self.dataObj))
        
    def createCorrCoef(self):
#         self.createCorrcoefAttributs()
        self.createCorrcoefMax()
#         self.createCorrcoefMin()
#         self.createCorrcoefPrice()
    
##################################################################################      
    def startCorrCoefTot(self):
        attributs,classes = self.loadData.createAttributsAndClassesCorrCoef()
#         for corrCoef in self.corrCoefAttributs:
#             corrCoef.setAttributsAndClassesCC(np.matrix(attributs))
        for corrCoef in self.corrCoefMinMaxPriceList:
            corrCoef.setAttributsAndClassesCC(np.matrix(attributs),np.array(classes))
            
        totDT = []
#         totDT.extend(self.corrCoefAttributs)
        totDT.extend(self.corrCoefMinMaxPriceList)
        for DT in totDT:
            DT.start()
        for DT in totDT:
            DT.join()    

##################################################################################
    def createAndStartMultipleCorrCoef(self):
        for companyNumber in xrange(90):
            self.initiateLoadingCompanies(companyNumber)
            self.initiateCorrCoefLists()
            
            self.createCorrCoef()
            self.startCorrCoefTot()
            print "finished computing companyNumer : "+str(companyNumber)
            
##################################################################################
sim = simulationCorrCoef()
sim.createAndStartMultipleCorrCoef()
print "End"

