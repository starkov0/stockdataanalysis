
#Function which calculates the cost of a transaction
#Input: nb_shares: the number of shares traded
#		price_per_share: price per share
#Output: cost: cost in dollars of the transaction
def calculateCost(nb_shares, price_per_share):
	
	cost=0

	#Flat Rate
	flat_rate=0.005*nb_shares
	if flat_rate < 1:
		flat_rate=1
	cost=flat_rate

	#Cost Plus
	cost_plus=0.0035*nb_shares
	cost=cost+cost_plus

	#Transaction Cost
	cost=cost+price_per_share*nb_shares

	return cost

print calculateCost(1000,20)