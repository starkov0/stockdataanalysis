# coding=utf-8
from DTInit import DTInit
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType

##################################################################################
def _pickle_method(method):
	func_name = method.im_func.__name__
	obj = method.im_self
	cls = method.im_class
	return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
	for cls in cls.mro():
		try:
			func = cls.__dict__[func_name]
		except KeyError:
			pass
		else:
			break
	return func.__get__(obj, cls)

##################################################################################

class useDTMin(DTInit):

	def __init__(self,varObj,dataObj,recv_input,send_output):
		super(useDTMin, self).__init__(varObj,dataObj)
		pickle(MethodType, _pickle_method, _unpickle_method)
		self.recv_input = recv_input
		self.send_output = send_output

##################################################################################
# DT -> test
	def getTree(self):
		self.tree = self.loadFile(self.varObj.min_Tree_File+self.dataObj.symbol)

	# classify a data with the DT
	def testDT(self,input_):
		return self.tree.predict(input_)[0]
	
	# classify week data
	def testWeekDT(self):
		week = self.varObj.testWeek
		self.dataObj.week = week
		startAnalysisValue = self.varObj.startAnalysisValue
		length = len(self.dataObj.weekData[week][0])-self.varObj.futur_time
		time = range(startAnalysisValue,length)

		pool1 = Pool(8)
		pool2 = Pool(8)

		async_results1 = [ pool1.apply_async(self.get40Input, (i,)) for i in time ]
		async_results2 = [ pool2.apply_async(self.getOutput, (i,)) for i in time ]
		map(ApplyResult.wait, async_results1)
		map(ApplyResult.wait, async_results2)
		input_=[r.get() for r in async_results1]
		realOutput=[r.get() for r in async_results2]

		async_results3 = [ pool1.apply_async(self.testDT, (i,)) for i in input_ ]
		map(ApplyResult.wait, async_results3)
		classifiedOutput=[r.get() for r in async_results3]

		self.writeCSV(realOutput, classifiedOutput, self.varObj.min_realClassified_File)

	def predictFutur(self):
		week = self.varObj.testWeek
		self.dataObj.week = week
		
		time = self.recv_input.recv()
		while time != "stop":
			input_ = self.get40Input(time)
			classifiedOutput = self.testDT(input_)
			self.send_output.send(classifiedOutput)
			time = self.recv_input.recv()

##################################################################################
	# gets outputs for the DT
	def getOutput(self,time):
		output = self.getOutputMin(time)
		return output