# coding=utf-8
from DecisionTree.DT_code.MinMaxPrice.DTInit import DTInit
import os.path
from sklearn import tree
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType
from indicatorToByteObj import indicatorToByteObj
from parsingTreeObj import parsingTreeObj

##################################################################################
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)
##################################################################################

class createDTSBN(DTInit):

    def __init__(self,varObj,dataObj):
        super(createDTSBN, self).__init__(varObj,dataObj)
        self.indicatorToByteObj = indicatorToByteObj()
        self.parsingTreeObj = parsingTreeObj()
        pickle(MethodType, _pickle_method, _unpickle_method)

##################################################################################
# decision Tree -> create network
    # creates a new DT or loads it from a file
    def createDT(self):
        if not os.path.exists(self.varObj.SBN_Tree_File):
            self.initiateDT()
            self.fillAttributsAndClassesDT()
            self.learnDT()
            self.saveFile(self.tree,self.varObj.SBN_Tree_File+self.dataObj.symbol)

    # initiates the decision Tree
    def initiateDT(self):
        self.tree = tree.DecisionTreeClassifier()

    def setAttributsAndClassesDT(self,attributs,classes):
        self.attributs = attributs
        self.classes = classes

    def fillAttributsAndClassesDT(self):
        h = 0
        nbTrainWeeks = self.varObj.nbTrainWeeks
        for companyNumber in xrange(self.varObj.trainCompanies_Start,self.varObj.trainCompanies_End):
            self.loadCompanyData(companyNumber)
            for week in xrange(nbTrainWeeks):
                h = self.fillAttributsAndClassesDTHelp(week,h)

    # learn data per week and put it into the DT
    def fillAttributsAndClassesDTHelp(self,week,h):
        self.dataObj.week = week
        futur_time = self.varObj.futur_time
        startAnalysisValue = self.varObj.startAnalysisValue
        length = len(self.dataObj.weekData[week][0])-futur_time
        time = range(startAnalysisValue,length)

        pool1 = Pool(8)
        async_results1 = [ pool1.apply_async(self.getBinaryInput, (i,)) for i in time ]
        map(ApplyResult.wait, async_results1)
        binaryInput=[r.get() for r in async_results1]
        
        pool2 = Pool(8)
        async_results1 = [ pool1.apply_async(self.getRealInput, (i,)) for i in binaryInput ]
        async_results2 = [ pool2.apply_async(self.getOutput, (i,)) for i in time ]
        map(ApplyResult.wait, async_results1)
        map(ApplyResult.wait, async_results2)
        input_=[r.get() for r in async_results1]
        output_=[r.get() for r in async_results2]

        self.attributs[h:h+len(time),:] = input_
        self.classes[h:h+len(time)] = output_

        h += len(time)
        return h

    # gets inputs as calculation of the different signals for the DecisionTree
    def getBinaryInput(self,time):
        macd, macdsignal = self.dataObj.getMACD(time)
        slowk = self.dataObj.getSTOCH(time)
        smak = self.dataObj.getSMAK(time)
        emap = self.dataObj.getEMAP(time)
        emav = self.dataObj.getEMAV(time)
        upperBB, middleBB, lowerBB = self.dataObj.getBBANDS(time)
        price = self.dataObj.getPRICE(time)
        volume = self.dataObj.getVOLUME(time)

        A1_A2 = self.indicatorToByteObj.getA1_A2(macd,macdsignal)
        A3 = self.indicatorToByteObj.getA3(emap,price)
        A4 = self.indicatorToByteObj.getA4(emav,volume)
        A5_A6 = self.indicatorToByteObj.getA5_A6(slowk,smak)
        A7_A8 = self.indicatorToByteObj.getA7_A8(upperBB,middleBB,lowerBB,price)

        binaryInput = self.processBinaryIntput(A1_A2,A3,A4,A5_A6,A7_A8)
        return binaryInput

    # takes a byte and creates an array of 8 bits -> Input
    def getRealInput(self,binaryInput):
        return_ = []
        for i in xrange(8):
            bit = binaryInput % 2
            return_.insert(0, bit)
            binaryInput = binaryInput / 2
        return return_

    # gets outputs for the DecisionTree
    def getOutput(self,binaryInput):
        outputCharacter = self.parsingTreeObj.parseBinaryNumber(binaryInput)
        if outputCharacter == 'N':
            output = 0
        elif outputCharacter == 'B':
            output = 1
        elif outputCharacter == 'S':
            output = -1
        else: 
            output = 101
        return output

    #�adds all the signals in an OR logical output
    def processBinaryIntput(self,a,b,c,d,e):
        num = a|b|c|d|e
        return num

    # add sample to DT
    def learnDT(self):
        self.tree.fit(self.attributs, self.classes)

        