# coding=utf-8
from createDTSBN import createDTSBN
from multiprocessing import Process

##################################################################################

class createDTSBNProcess(createDTSBN,Process):

	def __init__(self,varObj,dataObj):
		super(createDTSBNProcess, self).__init__(varObj,dataObj)

	def run(self):
		self.createDT()
		print "finished SBNDT creation"