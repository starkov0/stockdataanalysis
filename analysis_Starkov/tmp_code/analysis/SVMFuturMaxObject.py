# coding=utf-8
from dataObject import *
import numpy as np
from sklearn import svm
import cPickle as pickle
from scipy import *
from variablesObject import *
import Queue
import threading
import matplotlib.pyplot as plt

##################################################################################


class SVMFuturMaxObject(threading.Thread):

	def __init__(self,myQueue):
		super(SVMFuturMaxObject, self).__init__()
		self.myQueue = myQueue
		self.varObj = variablesObject()
		self.dataObj = dataObject(self.varObj.past_time,self.varObj.futur_time)
		self.loadIndexes()

		self.attributs = []
		self.classes = []

	def run(self):
		self.getSVM()
		self.prepareDataForClassifcation()
		self.testWeekSVM()

##################################################################################
# data object -> communicate with
	# allows to load the data of the next comapany
	def loadCompanyData(self,companyNumber):
		self.dataObj.loadCompanyData(companyNumber)

	def loadIndexes(self):
		self.dataObj.loadIndexData()

	def prepareDataForClassifcation(self):
		self.loadCompanyData(self.varObj.testCompany)

##################################################################################
# decision Tree -> create network
	# creates a new SVM or loads it from a file
	def getSVM(self):
		try: #if SVM exists -> lets load it!
			with open(self.varObj.max_SVM_File):
				self.loadSVM()

		except IOError: # SVM does not exist -> lets create it!
			self.initiateSVM()
			self.getAttributsClassesSVM()
			self.learnSVM()
			self.saveSVM()

	# initiates the decision Tree
	def initiateSVM(self):
		self.svm = svm.SVC(kernel='poly', degree=20)


	def getAttributsClassesSVM(self):
		# calculate length of data sets
		nbTrainWeeks = self.varObj.nbTrainWeeks
		longueur = self.varObj.max_Input_number
		hauteur = 0
		for companyNumber in xrange(self.varObj.trainCompanies_Start,self.varObj.trainCompanies_End):
			self.loadCompanyData(companyNumber)
			for week in xrange(nbTrainWeeks):
				hauteur += len(self.dataObj.weekData[week][0])-self.varObj.futur_time-self.varObj.startAnalysisValue
		# create data sets
		self.attributs = np.zeros([hauteur, longueur], dtype = float)
		self.classes = np.zeros([1,hauteur], dtype = float)[0]

		# fill data sets
		h = 0
		for companyNumber in xrange(self.varObj.trainCompanies_Start,self.varObj.trainCompanies_End):
			self.loadCompanyData(companyNumber)
			for week in xrange(nbTrainWeeks):
				h = self.getWeekAttributsClassesSVM(week,h,hauteur)

	# learn data per week and put it into the SVM
	def getWeekAttributsClassesSVM(self,week,h,hauteur):
		futur_time = self.varObj.futur_time
		startAnalysisValue = self.varObj.startAnalysisValue
		length = len(self.dataObj.weekData[week][0])-futur_time
		for time in xrange(startAnalysisValue,length):
			input_ = self.getInput(week,time)
			realOutput = self.getOutput(week,time)
			self.attributs[h,:] = input_
			self.classes[h] = realOutput
			h+=1
		return h

	# add sample to SVM
	def learnSVM(self):
		self.classes = self.classes * 1000
		print self.attributs
		print "\n\n\n\n\n"
		print self.classes
		print "\n\n\n\n\n"
		self.svm.fit(self.attributs, self.classes)

	# saves the SVM into a file
	def saveSVM(self):
		fileObject = open(self.varObj.max_SVM_File, 'w')
		pickle.dump(self.svm, fileObject)
		fileObject.close()

	# loads the SVM from a file
	def loadSVM(self):
		fileObject = open(self.varObj.max_SVM_File, 'r')
		self.svm = pickle.load(fileObject)
		fileObject.close()

##################################################################################
# SVM -> prepare data
	# gets inputs as calculation of the different signals for the SVM
	def getInput(self,week,time):
		derive1 = self.dataObj.getDerivePremiere(week,time)
		derive2 = self.dataObj.getDeriveSeconde(week,time)

		sam = self.dataObj.getSMApasttime(week,time)
		t3 = self.dataObj.getT3pasttime(week,time)
		adx = self.dataObj.getADXpasttime(week,time)
		atr = self.dataObj.getATRpasttime(week,time)
		rsi = self.dataObj.getRSIpasttime(week,time)
		roc = self.dataObj.getROCpasttime(week,time)
		slowk = self.dataObj.getSTOCHpasttime(week,time)
		smak = self.dataObj.getSMAKpasttime(week,time)
		emap = self.dataObj.getEMAPpasttime(week,time)
		emav = self.dataObj.getEMAVpasttime(week,time)

		input_ = np.concatenate([sam,t3,adx,atr,rsi,roc,emap,emav,slowk,smak,derive1,derive2])
		return input_

	# gets outputs for the SVM
	def getOutput(self,week,time):
		output = self.dataObj.getNextMaxValue(week,time)
		# output = self.dataObj.getNextPrice(week,time)
		return output

##################################################################################
# SVM -> test
	# classify a data with the SVM
	def testSVM(self,input_):
		return_value = self.svm.predict(input_)
		print return_value
		return return_value
	
	# classify week data
	def testWeekSVM(self):
		week = self.varObj.testWeek
		startAnalysisValue = self.varObj.startAnalysisValue
		length = len(self.dataObj.weekData[week][0])-self.varObj.futur_time
		real = np.zeros([1,length-self.varObj.startAnalysisValue], dtype = float)[0]
		classified = np.zeros([1,length-self.varObj.startAnalysisValue], dtype = float)[0]

		for time in xrange(startAnalysisValue,length):
			input_ = self.getInput(week,time)
			realOutput = self.getOutput(week,time)
			classifiedOutput = self.testSVM(input_)

			real[time-startAnalysisValue] = realOutput
			classified[time-startAnalysisValue] = classifiedOutput
		self.myQueue.put(real)
		self.myQueue.put(classified)
