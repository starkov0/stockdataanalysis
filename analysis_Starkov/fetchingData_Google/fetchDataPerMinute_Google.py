# coding=utf-8
import urllib,time,datetime
import matplotlib.pyplot as plt
import math

##################################################################################
nbCompaniesToFetch = 100
interval_seconds = 120
num_days = 50

DATE_FMT = '%Y-%m-%d'
TIME_FMT = '%H:%M:%S'

totCompanies_file = "../nasdaqCompanies/listNasdaq_tot.txt"
index_file = "../nasdaqCompanies/S&PDOW.txt"

historicalData_dir = "../historicalData/"
##################################################################################

class Google_Quotes():
  
  
  def __init__(self):
    self.symbol = ''
    self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
    self.header = []
    self.listSymbols = []
    self.listIndexes = []
    self.header = "DATE,CLOSE,HIGH,LOW,OPEN,VOLUME"



  # reads the names of the companies from a file and saves it in listSymbols
  def readCompanies(self):
    # f = open(totCompanies_file,"r")
    f = open(totCompanies_file,"r")
    i = 0
    while i < nbCompaniesToFetch:
      string = f.readline()
      if "^" in string: continue
      self.listSymbols.append(string.rstrip().replace("/","-"))
      i += 1
    f.close()



      # reads the names of the companies from a file and saves it in listSymbols
  def readIndexes(self):
    # f = open(totCompanies_file,"r")
    f = open(index_file,"r")
    i = 0
    while i < 2:
      string = f.readline()
      self.listIndexes.append(string.rstrip())
      i += 1
    f.close()



  # fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
  def fetchCompanyData(self):
    for symbol in self.listSymbols:
      self.symbol = symbol
      self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
      self.fetchData()
      self.write_csv()



  # fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
  def fetchIndexesData(self):
    for symbol in self.listIndexes:
      self.symbol = symbol
      self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
      self.fetchData()
      self.write_csv()



  # fetches data for one company at the time
  def fetchData(self):
    url_string = "http://www.google.com/finance/getprices?q={0}".format(self.symbol)
    url_string += "&i={0}&p={1}d&f=d,o,h,l,c,v".format(interval_seconds,num_days)
    csv = urllib.urlopen(url_string).readlines()
    for bar in xrange(7,len(csv)):
      if csv[bar].count(',')!=5: continue
      offset,close,high,low,open_,volume = csv[bar].split(',')
      if offset[0]=='a':
        day = float(offset[1:])
        offset = 0
      else:
        offset = float(offset)
      open_,high,low,close = [float(x) for x in [open_,high,low,close]]
      dt = datetime.datetime.fromtimestamp(day+(interval_seconds*offset))
      self.append(dt,open_,high,low,close,volume)



  # reads a CSV file
  def read_csv(self):
    self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
    for line in open(filename+".csv",'r'):
      ds,ts,open_,high,low,close,volume = line.rstrip().split(',')
      dt = datetime.datetime.strptime(ds+' '+ts,self.DATE_FMT+' '+self.TIME_FMT)
      self.append(dt,open_,high,low,close,volume)



  # appends the data to the object
  def append(self,dt,open_,high,low,close,volume):
    self.date.append(dt.date())
    self.time.append(dt.time())
    self.open_.append(float(open_))
    self.high.append(float(high))
    self.low.append(float(low))
    self.close.append(float(close))
    self.volume.append(int(volume))



  # writes a CSV
  def write_csv(self):
    with open(historicalData_dir+self.symbol+".csv",'a') as f:
      f.write(self.to_csv())



  # prepares lines that have to be written in a CSV file
  def to_csv(self):
    return ''.join(["{0},{1},{2:.2f},{3:.2f},{4:.2f},{5:.2f},{6}\n".format(
              self.date[bar].strftime(DATE_FMT),
              self.time[bar].strftime(TIME_FMT),
              self.open_[bar],
              self.high[bar],
              self.low[bar],
              self.close[bar],
              self.volume[bar])
              for bar in xrange(len(self.close))])
      

##################################################################################

getter = Google_Quotes()
getter.readCompanies()
getter.readIndexes()
getter.fetchCompanyData()
getter.fetchIndexesData()