# python unlink files that you dont need
import glob
import csv
from os.path import basename
from os import unlink

###############################
directoy = "../companiesData" # met le dir ICI!!!
numberLines = 27370 # met le nombre de lignes souhaite ICI!!!!
###############################
correctFiles = []

def writeCSV(data,name):
	resultFile = open(name,'wb')
	wr = csv.writer(resultFile, dialect='excel')
	for i in xrange(len(data)):
		wr.writerow([data[i],])
	resultFile.close()

files = glob.glob("./"+directoy+"/*")
for file in files:
    num_lines = sum(1 for line in open(file))
    if num_lines == numberLines:
      correctFiles.append(basename(file))
    else:
      unlink(file)

print correctFiles
writeCSV(correctFiles,'./companiesList2.txt')
