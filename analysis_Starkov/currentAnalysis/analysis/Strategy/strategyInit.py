import matplotlib.pyplot as plt 
from numpy import zeros
from csv import writer

class strategyInit(object):
    
    def __init__(self,recv_output,varObj,dataObj):
        super(strategyInit, self).__init__()
        self.recv_output = recv_output
        self.varObj = varObj
        self.dataObj = dataObj
        self.creteOutputLists()
        self.createTradingVariables()
        
##################################################################################
    def creteOutputLists(self):
        self.outputPrice = []
        self.outputMax = []

    def createTradingVariables(self):
        self.symbol = None
        self.nbStocks = []
        self.buyPrice = []
        self.sellPrice = []
        self.buyPrice_real = []
        self.sellPrice_real = []
        self.timeTot = []
        self.currentTime = 0
        self.timeBuy = 0
        self.timeSell = 0
        self.inTrade = 0
        self.time = 0
        
##################################################################################
    def setPorteMonnaieCommunication(self,send_ordre):
        self.send_ordre = send_ordre

##################################################################################
    def getDerive(self,data):
        tmpData = zeros([1,len(data)],dtype = float)[0]
        for i in xrange(1,len(data)):
            tmpData[i] = data[i] - data[i-1]
        return tmpData

##################################################################################   
    def plotFigure(self):
        tmpMax = self.getDerive(self.outputMax)
        name = self.varObj.strategy_plot_dir+self.dataObj.symbol[0:-4]+str(self.varObj.testDay)

        fig = plt.figure()
        plt.title('Filtered Data')
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        ax1.set_xlabel("Time (Minutes)")
        ax2.set_xlabel("Time (Minutes)")
        ax1.set_ylabel("Next Max (%)")
        ax2.set_ylabel("Current Price")

        ax1.plot(self.outputMax,'r')
        ax1.plot(tmpMax,'k')
        ax2.plot(self.outputPrice)
        plt.savefig(name,dpi=100)
        print "saved fig : "+name

##################################################################################   
    def writeTransactionCSV(self):
        resultFile = open(self.varObj.symbol_BuySell_dir+str(self.varObj.testDay)+".txt",'a')
        wr = writer(resultFile, dialect='excel')
        for i in xrange(len(self.timeTot)):
            wr.writerow([self.dataObj.symbol, self.buyPrice[i], self.sellPrice[i], self.buyPrice_real[i], self.sellPrice_real[i], self.nbStocks[i], self.timeTot[i],])
        
    def writeTreeAnalysisCurvesCSV(self):
        resultFile = open(self.varObj.treeAnalysisCurves_dir+str(self.varObj.testDay)+self.symbol,'a')
        wr = writer(resultFile, dialect='excel')
        wr.writerow([self.dataObj.symbol])
        wr.writerow(["PRICE"])
        wr.writerow(self.outputPrice)
        wr.writerow(["MAX"])
        wr.writerow(self.outputMax)
        wr.writerow(["DERIVE"])
        wr.writerow(self.getDerive(self.outputMax))
    
    