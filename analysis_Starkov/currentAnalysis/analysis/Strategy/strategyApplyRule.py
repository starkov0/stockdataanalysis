from Strategy.strategyAppendOutput import strategyAppendOutput


class strategyApplyRule(strategyAppendOutput):
    
    def __init__(self,recv_output,varObj,dataObj):
        super(strategyApplyRule, self).__init__(recv_output,varObj,dataObj)

##################################################################################    
    def rule(self):
        if len(self.outputMax) > 2:
            self.buyRule()  
            self.updateTimePrice()
            self.sellRule()          
                
    def ruleStop(self):
        if self.inTrade == 1:
            self.sellPrice.append(self.calculateCost(100,self.outputPrice[-1]))
            self.send_ordre.send(["sell", self.outputPrice[-1], self.symbol])
        #
        print self.symbol
        print "buy price : ", self.buyPrice
        print "sell price : ", self.sellPrice
        #
        self.send_ordre.send("stop")
        
##################################################################################    
    def buyRule(self):
        derive= self.outputMax[-1] - self.outputMax[-2]
        max1 = self.isUpperThanTreshhold(self.outputMax[-1], 0.5)
        max2 = self.isUpperThanTreshhold(derive, 0.3)
        # buy rule
        if (self.inTrade == 0) and max1 and max2:
            if derive > 100:
                derive = 100
            nbStocks_current = round(derive*100)
            self.send_ordre.send(["buy", nbStocks_current, self.outputPrice[-1], self.symbol])
            self.nbStocks.append(nbStocks_current)
            self.buyPrice.append(self.outputPrice[-1])
            self.buyPrice_real.append(self.calculateCost(self.nbStocks[-1], self.outputPrice[-1]))
            self.timeBuy = self.currentTime
            self.inTrade = 1
            self.time = 1
    
    def updateTimePrice(self):
        if self.inTrade == 1:
            self.time += 1
    
    def sellRule(self):
        if self.inTrade == 1:
            tmp = (self.outputPrice[-1]-self.buyPrice[-1])/self.buyPrice[-1]
            if (self.time > 60) or (tmp>0.004) or (tmp < -0.007): # 0.002 , -0.009 -> a faire 0.007 0.007
                self.sell()

    def sell(self):
        self.send_ordre.send(["sell", self.outputPrice[-1], self.symbol])
        self.timeSell = self.currentTime
        self.sellPrice.append(self.outputPrice[-1])
        self.sellPrice_real.append(self.calculateCost(self.nbStocks[-1], self.outputPrice[-1]))
        self.timeTot.append([self.timeBuy,self.timeSell])
        self.time = 0
        self.inTrade = 0
        
##################################################################################    
    def isUpperThanTreshhold(self, signal, seuil):
        if signal >= seuil:
            return 1
        else:
            return 0
        
##################################################################################        
    def calculateCost(self,nb_shares,price_per_share):
        flat_rate = 0.005*nb_shares #Flat Rate
        if flat_rate < 1:
            flat_rate = 1
        costTot = flat_rate
        
        cost_plus = 0.0035*nb_shares #Cost Plus
        costTot += cost_plus
        
        costTot += price_per_share * nb_shares #Transaction Cost
        return costTot
        