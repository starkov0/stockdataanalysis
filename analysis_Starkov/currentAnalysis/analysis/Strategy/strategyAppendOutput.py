from Strategy.strategyInit import strategyInit


class strategyAppendOutput(strategyInit):
    
    def __init__(self,recv_output,varObj,dataObj):
        super(strategyAppendOutput, self).__init__(recv_output,varObj,dataObj)
        
##################################################################################    
    def getOutput(self):
        outputTot = self.recv_output.recv()
        while outputTot != "stop":
            self.appendOutput(outputTot)
            self.rule()
            outputTot = self.recv_output.recv()
        self.ruleStop()
        
##################################################################################    
    def appendOutput(self,outputTot):
        self.outputMax.append(outputTot[0])
        self.outputPrice.append(outputTot[1])
        self.symbol = outputTot[2]
        self.currentTime = outputTot[3]
