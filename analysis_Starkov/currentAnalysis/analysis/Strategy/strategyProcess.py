from multiprocessing import Process
from Strategy.strategyApplyRule import strategyApplyRule

class strategyProcess(strategyApplyRule,Process):
    
    def __init__(self,recv_output,varObj,dataObj):
        super(strategyProcess, self).__init__(recv_output,varObj,dataObj)
              
##################################################################################  
    def run(self):
        self.getOutput()
        self.writeTreeAnalysisCurvesCSV()
#         self.writeTransactionCSV()
#         self.plotFigure()
        print "End Strategy"