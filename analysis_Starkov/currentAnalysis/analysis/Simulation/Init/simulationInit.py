# coding=utf-8
from copy import deepcopy
from Variables_Data.Variables.Variables import Variables
from Variables_Data.Data_Analysis.Indicator import Indicator
from CalculeRendement import CalculeRendement

import glob
from os import unlink

class simulationInit(object):

	def __init__(self):
		super(simulationInit, self).__init__()
		self.varObj = Variables()
		self.dataObj = Indicator(self.varObj)
		self.calculeRendement = CalculeRendement(self.varObj,self.dataObj)

##################################################################################	
	def initiateCreateDTLists(self):
		self.createDTList = []
		
	def initiateUseDTLists(self):
		self.useDTList = []		
		self.companyDT_list = []
		self.strategy_list = []
		self.send_input_list = []
		self.recv_output_list = []
		self.send_output_list = []
		self.dataObj_list = []
		self.porteMonnaie = None
		
##################################################################################
	def createVarDataObj(self,companyNumber):
		varObj = deepcopy(self.varObj)
		dataObj = Indicator(varObj)
		dataObj.loadCompanyData(companyNumber)
		return varObj, dataObj

##################################################################################	
	def calculeIndexes(self,start,end):
		return self.calculeRendement.getIndexesFromGoodGain(start,end)

##################################################################################
	def rmDTFiles(self):
		filelist = glob.glob(self.varObj.classifier_tree_dir+"*")
		for f in filelist:
			unlink(f)
			
	def rmCSVFiles(self):
		filelist = glob.glob(self.varObj.classifier_data_dir+"*.csv")
		for f in filelist:
			unlink(f)

