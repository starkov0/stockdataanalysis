# coding=utf-8
from multiprocessing import Pipe
from Simulation.CreateUse.simulationCU_Strat_PM import simulationCU_Strat_PM

class simulation_Strat_PM_comm(simulationCU_Strat_PM):

    def __init__(self):
        super(simulation_Strat_PM_comm, self).__init__()
            
##################################################################################
    def recvOutputPredicted(self,time):
        outputTOT = []
        for i in xrange(len(self.recv_output_list)):
            max_ = self.recv_output_list[i][0].recv()
            tot = [max_, self.dataObj_list[i].getPrice(time),self.dataObj_list[i].symbol, time]
            outputTOT.append(tot)
        return outputTOT
    
    def sendOutputToStrategy(self,outputTOT):
        for i in xrange(len(self.send_output_list)):
            self.send_output_list[i].send(outputTOT[i])
        
##################################################################################
    def sendIntputToPredict(self,time):
        for send_input_per_company in self.send_input_list:
            send_input_per_company[0].send(time)
        
    def stopInputCommunication(self):
        for send_input_per_company in self.send_input_list:
            send_input_per_company[0].send("stop")
            
    def stopOutputCommunication(self):
        for send_output in self.send_output_list:
            send_output.send("stop")
        
##################################################################################            
    def setSPMCommunication(self):
        recv_ordre, send_ordre = Pipe()
        self.porteMonnaie.setStrategieCommunication(recv_ordre)
        for strategy in self.strategy_list:
            strategy.setPorteMonnaieCommunication(send_ordre)
