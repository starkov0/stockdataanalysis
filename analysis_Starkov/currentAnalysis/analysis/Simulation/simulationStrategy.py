# coding=utf-8
from Simulation.simulation_Strat_PM_comm import simulation_Strat_PM_comm
import time
import os

##################################################################################

start = 0
end = 900

class simulationStrategy(simulation_Strat_PM_comm):

    def __init__(self):
        super(simulationStrategy, self).__init__()

##################################################################################            
    def createTimeVector(self):
        self.dataObj.loadCompanyData(0)
        startAnalysisValue = self.varObj.startAnalysisValue
        length = len(self.dataObj.dayData[self.varObj.testDay][0])
        time = range(startAnalysisValue,length)
        return time

##################################################################################

    def creationOfTreesHelp(self,companyNumber,startDay,endDay):
        print "CREATE DT", companyNumber, endDay
        varObj, dataObj = self.createVarDataObj(companyNumber)
        varObj.trainDay_Start = startDay
        varObj.trainDay_End = endDay    
        varObj.testDay = endDay    
        self.createDT(varObj, dataObj)
        
    def usageOfTreesHelp(self,companyNumber,testDay,name):
        print "USE DT", companyNumber, testDay
        varObj, dataObj = self.createVarDataObj(companyNumber)
        dataObj.day = testDay
        varObj.testDay = testDay
        self.initiateUseDTLists()
        self.createUseDT(varObj, dataObj)
        self.startUseDT()
        self.createTOTStratPM(varObj, dataObj,name)
        self.startTOTStratPM()
        timeList = self.createTimeVector()
        for time in timeList:
            self.sendIntputToPredict(time)
            outputTot = self.recvOutputPredicted(time)
            self.sendOutputToStrategy(outputTot)
        #
        self.stopInputCommunication()
        self.stopOutputCommunication()

##################################################################################

    def creationOfTrees(self,dayS,dayE):
        self.initiateCreateDTLists()
        i = 0
        for company in xrange(start,end):
            for day in xrange(dayS,dayE):
                if not os.path.exists(self.varObj.max_Tree_File+self.dataObj.companiesList[company][0:-4]+str(day)):
                    self.creationOfTreesHelp(company,day-10,day)
                    i+=1
                    if i % 10 == 0:
                        self.startCreateDT()
                        self.joinCreateDT()
                        self.initiateCreateDTLists()
        self.startCreateDT()
        self.joinCreateDT()                                
                                
    def usageOfTrees(self,dayE,dayS):
        name = self.varObj.gain_dir
        for day in xrange(dayE,dayS):
            if not os.path.exists(self.varObj.gain_dir+"gain"+str(day)+".csv"):
                for companyNumber in xrange(start,end):
                    self.usageOfTreesHelp(companyNumber,day,name)
                
    def realUsageOfTrees(self,indexes,day):
        name = self.varObj.realGain_dir
        if not os.path.exists(self.varObj.realGain_dir+"gain"+str(day)+".csv"):
            for companyNumber in indexes:
                self.usageOfTreesHelp(companyNumber,day,name)
    
# creation -> 14-20, 20-25, ...
    def creation_usage_3_1(self):
        numberOfDayToLook = 3
        start = 65
#         for day in xrange(13,69):
#             self.creationOfTrees(day-numberOfDayToLook, day+1)
        for day in xrange(65,69):
            self.usageOfTrees(day-1, day)
#             self.usageOfTrees(day-numberOfDayToLook, day)
#         for day in xrange(13,69):
#             indexes = self.calculeIndexes(day-numberOfDayToLook, day)
#             print "INDEXES !!! : ", len(indexes), indexes
#             self.realUsageOfTrees(indexes,day)

##################################################################################
sim = simulationStrategy()
# sim.rmDTFiles()
sim.creation_usage_3_1()
# time.sleep(5)
# os.system("pkill -9 Python")
print "End Trading"