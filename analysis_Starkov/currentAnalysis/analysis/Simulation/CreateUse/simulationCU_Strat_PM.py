# coding=utf-8
from multiprocessing import Pipe
from Simulation.CreateUse.simulationCU_DT import simulationCU_DT
from Strategy.strategyProcess import strategyProcess
from PorteMonnaie.porteMonnaieProcess import porteMonnaieProcess


class simulationCU_Strat_PM(simulationCU_DT):

    def __init__(self):
        super(simulationCU_Strat_PM, self).__init__()

##################################################################################   
    def createTOTStratPM(self,varObj, dataObj, name):
        self.createStrategy(varObj, dataObj)
        self.createPorteMonnaie(varObj, dataObj, name)
        self.setSPMCommunication()
        
    def startTOTStratPM(self):
        self.startPorteMonnaie()
        self.startStrategy()
   
##################################################################################   
    def createStrategy(self,varObj, dataObj):
        recv_output, send_output = Pipe()
        self.strategy_list.append(strategyProcess(recv_output,varObj,dataObj))
        self.send_output_list.append(send_output)
    
    def startStrategy(self):
        for strategy in self.strategy_list:
            strategy.start()
        
##################################################################################            
    def createPorteMonnaie(self,varObj, dataObj, name):
        self.porteMonnaie = porteMonnaieProcess(varObj, dataObj, name)
        
    def startPorteMonnaie(self):
        self.porteMonnaie.start()
        
##################################################################################            
    def joinTOTStratPM(self):
        self.porteMonnaie.join()
        for strategy in self.strategy_list:
            strategy.join()
