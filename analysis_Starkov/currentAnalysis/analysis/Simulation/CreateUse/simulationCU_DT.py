# coding=utf-8
from numpy import array, matrix
from multiprocessing import Pipe
from Simulation.Init.simulationInit import simulationInit

from DecisionTree.MinMax_Tree.createDTMaxProcess import createDTMaxProcess
from DecisionTree.MinMax_Tree.createDTMinProcess import createDTMinProcess
from DecisionTree.MinMax_Tree.useDTMaxProcess import useDTMaxProcess
from DecisionTree.MinMax_Tree.useDTMinProcess import useDTMinProcess
from DecisionTree.SBN_Tree.useDTSBNProcess import useDTSBNProcess

class simulationCU_DT(simulationInit):

    def __init__(self):
        super(simulationCU_DT, self).__init__()
        
##################################################################################
# create objects -> DT
    def createDT(self,varObj, dataObj):
        attributs,classes = dataObj.createAttributsAndClassesDT()
        #
        self.createDTMax(varObj,dataObj,matrix(attributs),array(classes))
#         self.createDTMin(varObj,dataObj,matrix(attributs),array(classes))
        
    def createDTMax(self,varObj,dataObj,attributs,classes):
        DT = createDTMaxProcess(varObj,dataObj)
        DT.setAttributsAndClassesDT(attributs,classes)
        self.createDTList.append(DT)
        
#     def createDTMin(self,varObj,dataObj,attributs,classes):
#         DT = createDTMinProcess(varObj,dataObj)
#         DT.setAttributsAndClassesDT(attributs,classes)
#         self.createDTList.append(DT)
        
##################################################################################
# start classifications
    def startCreateDT(self):
        for DT in self.createDTList:
            DT.start()
            
    def joinCreateDT(self):
        for DT in self.createDTList:
            DT.join()
        
##################################################################################
# USE objects -> DT   
    def createUseDT(self,varObj, dataObj):
        #
        self.startUseDTMax(varObj,dataObj)
#         self.startUseDTMin(varObj,dataObj)
#         self.startUseDTSBN(varObj,dataObj)
        #
        self.dataObj_list.append(dataObj)
        self.send_input_list.append([self.max_send_input]) #,self.min_send_input,self.SBN_send_input])
        self.recv_output_list.append([self.max_recv_output]) #,self.min_recv_output,self.SBN_recv_output])

    def startUseDTMax(self,varObj,dataObj):
        recv_input, self.max_send_input = Pipe()
        self.max_recv_output, send_output = Pipe()
        self.useDTList.append(useDTMaxProcess(varObj,dataObj,recv_input,send_output))
# 
#     def startUseDTMin(self,varObj,dataObj):
#         recv_input, self.min_send_input = Pipe()
#         self.min_recv_output, send_output = Pipe()
#         self.useDTList.append(useDTMinProcess(varObj,dataObj,recv_input,send_output))
# 
#     def startUseDTSBN(self,varObj,dataObj):
#         recv_input, self.SBN_send_input = Pipe()
#         self.SBN_recv_output, send_output = Pipe()
#         self.useDTList.append(useDTSBNProcess(varObj,dataObj,recv_input,send_output))

##################################################################################
# start classifications            
    def startUseDT(self):
        for DT in self.useDTList:
            DT.start()
        self.companyDT_list.append(self.useDTList)
        self.useDTList = []
            
    def joinUseDT(self):
        for DT in self.useDTList:
            DT.join()
            