class Node(object):

	def __init__(self):
		self.left = None
		self.right = None
		self.data = None

	def createTree(self):
		B = Node()
		B.data = "B"
		S = Node()
		S.data = "S"
		N = Node()
		N.data = "N"

		##################################################################################
		root = Node()
		A2 = root
		A2.data = "2"
		A2.left = B
		A2.right = Node()

		A1 = A2.right
		A1.data = "1"
		A1.left = S
		A1.right = Node()


		A5 = A1.right
		A5.data = "5"
		A5.left = Node()
		A5.right = Node()

		A7 = A5.left
		A7.data = "7"
		A7.left = Node()
		A7.right = S

		A3 = A7.left
		A3.data = "3"
		A3.left = Node()
		A3.right = S

		A8 = A3.left
		A8.data = "8"
		A8.left = Node()
		A8.right = N

		A4 = A8.left
		A4.data = "4"
		A4.left = S
		A4.right = N
		##################################################################################
		A4 = A5.right
		A4.data = "4"
		A4.left = Node()
		A4.right = Node()

		A8 = A4.left
		A8.data = "8"
		A8.left = Node()
		A8.right = Node()

		A6 = A8.left
		A6.data = "6"
		A6.left = B
		A6.right = Node()

		A7 = A6.right
		A7.data = "7"
		A7.left = B
		A7.right = N
		##################################################################################
		A7 = A8.right
		A7.data = "7"
		A7.left = Node()
		A7.right = Node()

		A6 = A7.left
		A6.data = "6"
		A6.left = Node()
		A6.right = N

		A3 = A6.left
		A3.data = "3"
		A3.left = B
		A3.right = N

		A6 = A7.right
		A6.data = "6"
		A6.left = N
		A6.right = S
		##################################################################################
		A8 = A4.right
		A8.data = "8"
		A8.left = Node()
		A8.right = N

		A7 = A8.left
		A7.data = "7"
		A7.left = Node()
		A7.right = N

		A6 = A7.left
		A6.data = "6"
		A6.left = B
		A6.right = N

		return root


class parsingTreeObj(Node):

	def __init__(self):
		super(parsingTreeObj,self).__init__()
		self.root = self.createTree()

	def parseBinaryNumber(self,binNumber):
		node = self.root
		data = node.data
		while data not in ['B','S','N']:
			binValue = self.getValueAtPosition(binNumber,int(data))
			if binValue == 0:
				node = node.right
			else:
				node = node.left
			data = node.data
		return data

	def getValueAtPosition(self,binNumber,position):
		mask = bin(2**(8-position))
		binValue = int(binNumber) & int(mask,2)
		binValue = binValue >> (8-position)
		return binValue