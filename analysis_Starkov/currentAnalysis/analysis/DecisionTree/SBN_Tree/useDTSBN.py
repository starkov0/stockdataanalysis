# coding=utf-8
from DecisionTree.MinMax_Tree.DTInit import DTInit
from indicatorToByteObj import indicatorToByteObj
from parsingTreeObj import parsingTreeObj

##################################################################################
class useDTSBN(DTInit):

	def __init__(self,varObj,dataObj,recv_input,send_output):
		super(useDTSBN, self).__init__(varObj,dataObj)
		self.indicatorToByteObj = indicatorToByteObj()
		self.parsingTreeObj = parsingTreeObj()
		self.recv_input = recv_input
		self.send_output = send_output

##################################################################################
	def predictFutur(self):
		time = self.recv_input.recv()
		while time != "stop":
			input_ = self.getBinaryInput(time)
			classifiedOutput = self.getRealOutput(input_)
			self.send_output.send(classifiedOutput)
			time = self.recv_input.recv()
			
##################################################################################
	# gets inputs as calculation of the different signals for the DecisionTree
	def getBinaryInput(self,time):
		macd, macdsignal = self.dataObj.getMACD(time)
		slowk = self.dataObj.getSTOCH(time)
		smak = self.dataObj.getSMAK(time)
		emap = self.dataObj.getEMAP(time)
		emav = self.dataObj.getEMAV(time)
		upperBB, middleBB, lowerBB = self.dataObj.getBBANDS(time)
		price = self.dataObj.getPRICE(time)
		volume = self.dataObj.getVOLUME(time)

		A1_A2 = self.indicatorToByteObj.getA1_A2(macd,macdsignal)
		A3 = self.indicatorToByteObj.getA3(emap,price)
		A4 = self.indicatorToByteObj.getA4(emav,volume)
		A5_A6 = self.indicatorToByteObj.getA5_A6(slowk,smak)
		A7_A8 = self.indicatorToByteObj.getA7_A8(upperBB,middleBB,lowerBB,price)

		binaryInput = self.processBinaryIntput(A1_A2,A3,A4,A5_A6,A7_A8)
		return binaryInput

	# adds all the signals in an OR logical output
	def processBinaryIntput(self,a,b,c,d,e):
		num = a|b|c|d|e
		return num

	# takes a byte and creates an array of 8 bits -> Input
	def getRealInput(self,binaryInput):
		return_ = []
		for i in xrange(8):
			bit = binaryInput % 2
			return_.insert(0, bit)
			binaryInput = binaryInput / 2
		return return_

	# gets outputs for the DecisionTree
	def getRealOutput(self,binaryInput):
		outputCharacter = self.parsingTreeObj.parseBinaryNumber(binaryInput)
		if outputCharacter == 'N':
			output = 0
		elif outputCharacter == 'B':
			output = 1
		elif outputCharacter == 'S':
			output = -1
		else: output = 101
		return output
