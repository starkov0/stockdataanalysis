# coding=utf-8
class indicatorToByteObj():

	# returns the sign of the value X
	def sign(self,x): 
		returnValue = 1 if x >= 0 else -1
		return returnValue

	def getA1_A2(self,MACD,MACDSIGNAL):
		diffMACD0 = MACD[0]-MACDSIGNAL[0]
		diffMACD1 = MACD[1]-MACDSIGNAL[1]
		diffMACD2 = MACD[2]-MACDSIGNAL[2]
		if diffMACD1 == 0:
			if self.sign(diffMACD2) != self.sign(diffMACD1):
				if diffMACD2 < 0:
					return 0b10000000
				else:
					return 0b01000000
			else:
				return 0b00000000
		else:
			if self.sign(diffMACD2) != self.sign(diffMACD0):
				if diffMACD2 < 0:
					return 0b10000000
				else:
					return 0b01000000
			else:
				return 0b00000000

	def getA3(self,EMAP,PRICE):
		if PRICE[0] > EMAP[0]:
			return 0b00100000
		else:
			return 0b00000000

	def getA4(self,EMAV,VOLUME):
		if VOLUME[0] > EMAV[0]:
			return 0b00010000
		else:
			return 0b00000000

	def getA5_A6(self,K,SMAK):
		diffK0 = K[0]-SMAK[0]
		diffK1 = K[1]-SMAK[1]
		diffK2 = K[2]-SMAK[2]
		if diffK1 == 0:
			if self.sign(diffK2) != self.sign(diffK1):
				if diffK2 < 0:
					return 0b00001000
				else:
					return 0b00000100
			else:
				return 0b00000000
		else:
			if self.sign(diffK2) != self.sign(diffK0):
				if diffK2 < 0:
					return 0b00001000
				else:
					return 0b00000100
			else:
				return 0b00000000

	def getA7_A8(self,UPBB,MIDBB,LOWBB,PRICE):
		if PRICE[0] > LOWBB[0]:
			return 0b00000000
		elif LOWBB[0] < PRICE[0] < MIDBB[0]:
			return 0b00000001
		elif MIDBB[0] < PRICE[0] < UPBB[0]:
			return 0b00000010
		else:
			return 0b00000011
