# coding=utf-8
from useDTMin import useDTMin
from multiprocessing import Process

##################################################################################

class useDTMinProcess(useDTMin,Process):

	def __init__(self,varObj,dataObj,recv_input,send_output):
		super(useDTMinProcess, self).__init__(varObj,dataObj,recv_input,send_output)

	def run(self):
		self.getTree()
		self.predictFutur()
		print "finished MinDT usage"