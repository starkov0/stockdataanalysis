# coding=utf-8
from createDTMax import createDTMax
from multiprocessing import Process

##################################################################################

class createDTMaxProcess(createDTMax,Process):

	def __init__(self,varObj,dataObj):
		super(createDTMaxProcess, self).__init__(varObj,dataObj)
		self.daemon = True

	def run(self):
		self.createDT()
		print "finished MaxDT creation : "+self.dataObj.symbol
