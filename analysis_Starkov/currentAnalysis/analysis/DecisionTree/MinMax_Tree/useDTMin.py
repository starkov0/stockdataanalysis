# coding=utf-8
from DTInit import DTInit

##################################################################################
class useDTMin(DTInit):

	def __init__(self,varObj,dataObj,recv_input,send_output):
		super(useDTMin, self).__init__(varObj,dataObj)
		self.recv_input = recv_input
		self.send_output = send_output

##################################################################################
# DT -> test
	def getTree(self):
		self.tree = self.loadFile(self.varObj.min_Tree_File+self.dataObj.symbol[0:-4]+str(self.varObj.testDay))

	# classify a data with the DT
	def testDT(self,input_):
		return self.tree.predict(input_)[0]
	
	def predictFutur(self):
		time = self.recv_input.recv()
		while time != "stop":
			input_ = self.get40Input(time)
			classifiedOutput = self.testDT(input_)
			self.send_output.send(classifiedOutput)
			time = self.recv_input.recv()