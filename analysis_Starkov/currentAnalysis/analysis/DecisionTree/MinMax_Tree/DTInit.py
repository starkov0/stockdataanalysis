# coding=utf-8
import cPickle as pickle
from numpy import concatenate
import csv

##################################################################################

class DTInit(object):

	def __init__(self,varObj,dataObj):
		super(DTInit, self).__init__()
		self.attributs = []
		self.classes = []
		self.varObj = varObj
		self.dataObj = dataObj

##################################################################################
# data object -> communicate with
	# allows to load the data of the next comapany
	def loadCompanyData(self,companyNumber):
		self.dataObj.loadCompanyData(companyNumber)

	def prepareDataForClassifcation(self):
		self.loadCompanyData(self.varObj.testCompany)

##################################################################################
# decision Tree -> save - load
	def saveFile(self,data,name):
		fileObject = open(name, 'wb')
		pickle.dump(data, fileObject)
		fileObject.close()

	# loads the DT from a file
	def loadFile(self,name):
		fileObject = open(name, 'rb')
		data = pickle.load(fileObject)
		fileObject.close()
		return data
	
	def writeCSV(self,data1,data2,name):
		resultFile = open(name,'wb')
		wr = csv.writer(resultFile, dialect='excel')
		for i in xrange(len(data1)):
			wr.writerow([data1[i],data2[i],])
		resultFile.close()
		
	def writeCSV1(self,data,name):
		resultFile = open(name,'wb')
		wr = csv.writer(resultFile, dialect='excel')
		for i in xrange(len(data)):
			wr.writerow([data[i],])
		resultFile.close()
		
	def readCSV(self,name):
		data1 = []
		data2 = []
		resultFile = open(name,'rb')
		rd = csv.reader(resultFile, dialect='excel')
		for row in rd:
			data1.append(row[0])
			data2.append(row[1])
		resultFile.close()
		return data1, data2
	
	def readCSV1(self,name):
		data = []
		resultFile = open(name,'rb')
		rd = csv.reader(resultFile, dialect='excel')
		for row in rd:
			data.append(row[0])
		resultFile.close()
		return data

##################################################################################
# DT -> prepare data
	# gets inputs as calculation of the different signals for the DT
	def get40Input(self,time):
		
		
		
# 		emav = self.dataObj.getEMAVpasttime(time)
		emap = self.dataObj.getEMAPpasttime(time)
# 		dema = self.dataObj.getDEMApasttime(time)
		ht_trendline = self.dataObj.getHT_TRENDLINEpasttime(time)
		ma = self.dataObj.getMApasttime(time)
		midpoint = self.dataObj.getMIDPOINTpasttime(time)
		midprice = self.dataObj.getMIDPRICEpasttime(time)
		t3 = self.dataObj.getT3pasttime(time)
 		tema = self.dataObj.getTEMApasttime(time)
		trima = self.dataObj.getTRIMApasttime(time)
 		wma = self.dataObj.getWMApasttime(time)
#  		apo = self.dataObj.getAPOpasttime(time)
#  		aroonosc = self.dataObj.getAROONOSCpasttime(time)
#  		cmo = self.dataObj.getCMOpasttime(time)
#  		mfi = self.dataObj.getMFIpasttime(time)
#  		minus_di = self.dataObj.getMINUS_DIpasttime(time)
#  		minus_dm = self.dataObj.getMINUS_DMpasttime(time)
#  		mom = self.dataObj.getMOMpasttime(time)
#  		plus_dm = self.dataObj.getPLUS_DMpasttime(time)
#  		ppo = self.dataObj.getPPOpasttime(time)
#  		rocp = self.dataObj.getROCPpasttime(time)
#  		rocr = self.dataObj.getROCRpasttime(time)
#  		rocr100 = self.dataObj.getROCR100pasttime(time)
#  		rsi = self.dataObj.getRSIpasttime(time)
#  		smak = self.dataObj.getSMAKpasttime(time)
#  		willr = self.dataObj.getWILLRpasttime(time)
#  		atr = self.dataObj.getATRpasttime(time)
#  		natr = self.dataObj.getNATRpasttime(time)
#  		trange = self.dataObj.getTRANGEpasttime(time)

# 		input_ = concatenate([emav,emap,dema,ht_trendline,ma,midpoint,midprice,t3,tema,trima,wma,
#    			apo,aroonosc,cmo,mfi,minus_di,minus_dm,mom,plus_dm,ppo,rocp,rocr,rocr100,rsi,smak,willr,
#  			atr,natr,trange])*10
 		input_ = concatenate([emap,ht_trendline,ma,midpoint,midprice,t3,tema,trima,wma])*10
		return input_

	# gets outputs for the DT
	def getOutputMax(self,time):
		output = self.dataObj.getNextMaxValue(time)
		return output

		# gets outputs for the DecisionTree
	def getOutputMin(self,time):
		output = self.dataObj.getNextMinValue(time)
		return output
	