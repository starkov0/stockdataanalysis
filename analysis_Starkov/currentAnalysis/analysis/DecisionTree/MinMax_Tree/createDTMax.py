# coding=utf-8
from DTInit import DTInit
import os.path
from sklearn import tree
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType
# from numpy import corrcoef, array

##################################################################################
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)
##################################################################################

class createDTMax(DTInit):

    def __init__(self,varObj,dataObj):
        super(createDTMax, self).__init__(varObj,dataObj)
        pickle(MethodType, _pickle_method, _unpickle_method)

##################################################################################
# decision Tree -> create network
    # creates a new DT or loads it from a file
    def createDT(self):
        if not os.path.exists(self.varObj.max_Tree_File+self.dataObj.symbol[0:-4]+str(self.varObj.testDay)):
            self.initiateDT()
            self.fillAttributsAndClassesDT()
#             self.correlation()
            self.learnDT()
            self.saveFile(self.clf,self.varObj.max_Tree_File+self.dataObj.symbol[0:-4]+str(self.varObj.testDay))
# 
#     def correlation(self):
#         corrcoeffTot = []
#         for i in xrange(self.varObj.input_number):
#             tmp = corrcoef(array(self.attributs[:,i]).reshape(-1,),array(self.classes))[0,1]
#             corrcoeffTot.append(abs(tmp))
#         self.writeCSV1(corrcoeffTot, self.varObj.corrcoeff_dir+self.dataObj.symbol+str(self.varObj.testDay)+".txt")

    # initiates the decision Tree
    def initiateDT(self):
        self.clf = tree.DecisionTreeClassifier(min_samples_leaf=70, min_samples_split=50)

    def setAttributsAndClassesDT(self,attributs,classes):
        self.attributs = attributs
        self.classes = classes

    def fillAttributsAndClassesDT(self):
        h = 0
        for day in xrange(self.varObj.trainDay_Start,self.varObj.trainDay_End):
            h = self.fillAttributsAndClassesDTHelp(day,h)

    # learn data per week and put it into the DT
    def fillAttributsAndClassesDTHelp(self,day,h):
        self.dataObj.day = day
        startAnalysisValue = self.varObj.startAnalysisValue
        length = len(self.dataObj.dayData[day][0])-self.varObj.futur_time
        time = range(startAnalysisValue,length)
        #
        pool1 = Pool(8)
        pool2 = Pool(8)
        async_results1 = [ pool1.apply_async(self.get40Input, (i,)) for i in time ]
        async_results2 = [ pool2.apply_async(self.getOutput, (i,)) for i in time ]
        map(ApplyResult.wait, async_results1)
        map(ApplyResult.wait, async_results2)
        input_=[r.get() for r in async_results1]
        output_=[r.get() for r in async_results2]
        #
        self.attributs[h:h+len(time),:] = input_
        self.classes[h:h+len(time)] = output_
        #
        h += len(time)
        return h

    # add sample to DT
    def learnDT(self):
        self.clf.fit(self.attributs, self.classes)

        # gets outputs for the DT
    def getOutput(self,time):
        output = self.getOutputMax(time)
        return output
