# coding=utf-8
from useDTMax import useDTMax
from multiprocessing import Process

##################################################################################

class useDTMaxProcess(useDTMax,Process):

	def __init__(self,varObj,dataObj,recv_input,send_output):
		super(useDTMaxProcess, self).__init__(varObj,dataObj,recv_input,send_output)

	def run(self):
		self.getTree()
		self.predictFutur()
		print "finished MaxDT usages : "+self.dataObj.symbol
