# coding=utf-8
from createDTMin import createDTMin
from multiprocessing import Process

##################################################################################

class createDTMinProcess(createDTMin,Process):

	def __init__(self,varObj,dataObj):
		super(createDTMinProcess, self).__init__(varObj,dataObj)

	def run(self):
		self.createDT()
		print "finished MinDT creation : "+self.dataObj.symbol