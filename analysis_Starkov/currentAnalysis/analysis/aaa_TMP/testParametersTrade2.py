import csv
from os import listdir, getcwd
from numpy import sum, array, meshgrid, linspace, arange, matrix, mean
import matplotlib.pyplot as plt
import pickle
from multiprocessing import Pool
import functools
import pprint
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.colors import LogNorm
import numpy as np

#####################################################################################
# READ - WRITE
def readCSV(name):
    data = []
    resultFile = open(name,'rb')
    rd = csv.reader(resultFile, dialect='excel')
    for row in rd:
        data.append(row)
    return data

def writePickle(data,name):
    pickle.dump( data, open( "./result/data/"+name, "wb" ) )

def readPickle(name):
    return pickle.load(open( "./result/data/"+name, "rb" ))

#####################################################################################
# AFFICHAGE
def showBestConfiguration(z,n,title):
    fig, ax = plt.subplots()
    phi_m = range(0,n)
    phi_p = range(0,n)
    x,y = meshgrid(phi_p, phi_m)
    p = ax.pcolor(x, y, z)
    cb = fig.colorbar(p, ax=ax)
    plt.title(title)
    plt.xlabel("maxOutput")
    plt.ylabel("maxDerive")
    plt.savefig("./result/img/"+title)
    
def plot3D(mat,title,n):
    mat = matrix(mat)
    X, Y = np.mgrid[:n, :n]
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1, projection='3d')
    surf = ax.plot_surface(X,Y,mat, rstride=1, cstride=1, cmap = plt.get_cmap('copper'))
    ax.set_xlabel("MAAAAX")
    ax.set_ylabel("MIIIIN")
#     plt.show()
    plt.savefig("./result/plot3D/"+title)
    
def simplePlot(list_):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(list_)
    plt.show()

#####################################################################################
# LIST CREATION
def createBigList(n):
    return [[[[[] for x in xrange(n)] for x in xrange(n)] for x in xrange(n)] for x in xrange(n)]
    
#####################################################################################
# LIST TO NUMERAL
def listNbTransactionTotalToNumerical(n, nbTransaction, total):
    for j in xrange(n):
        for k in xrange(n):
            for l in xrange(n):
                for m in xrange(n):
                    nbTransaction[j][k][l][m] = len(total[j][k][l][m])
                    if total[j][k][l][m] == []:
                        total[j][k][l][m] = 0
                    else:
                        total[j][k][l][m] = sum(total[j][k][l][m])
    return nbTransaction, total

def listToNumerical(n, list_):
    for j in xrange(n):
        for k in xrange(n):
            for l in xrange(n):
                for m in xrange(n):
                    if len(list_[j][k][l][m]) == 0:
                        list_[j][k][l][m] = 0
                    else:
                        list_[j][k][l][m] = sum(list_[j][k][l][m])
    return list_

#####################################################################################
# MAIN ALGORITHM -> THREADING
def calculate(file_, list_):
    n = 10
    a = readCSV(getcwd()+"/../Results/treeAnalysisCurves/"+list_[file_])
    a[2] =  list(array(a[2], dtype = float))
    a[4] =  list(array(a[4], dtype = float))
    a[6] =  list(array(a[6], dtype = float))
    total = [[[[[] for x in xrange(n)] for x in xrange(n)] for x in xrange(n)] for x in xrange(n)]
    maxOutput = linspace(0.1, 1, 10)
    maxDerive = linspace(0.1, 1, 10)
    maxFrac = linspace(0.0005, 0.04, 10)
    minFrac = linspace(-0.0005, -0.04, 10)
    for j in xrange(n):
        for k in xrange(n):
            maxTMP = maxOutput[j]
            maxDeriveTMP = maxDerive[k]
            lenList = len(a[2])
            for l in xrange(n):
                for m in xrange(n):
                    maxFracTMP = maxFrac[l]
                    minFracTMP = minFrac[m]
                    trade = False
                    buyUniquePrice = None
                    buyPrice = None
                    buyTaxe = None
                    sellPrice = None
                    sellTaxe = None
                    nb_actions = None
                    time = 0
                    i = 0
                    totalTMP = []
                    while i < lenList-1:
                        if not trade:
                            if a[4][i] > maxTMP and a[6][i] > maxDeriveTMP: 
                                trade = True
                                nb_actions = 100
                                if a[2][i]*nb_actions > 10000:
                                    nb_actions = int(10000/a[2][i])-1
                                buyUniquePrice = a[2][i]
                                buyPrice = a[2][i]*nb_actions
                                buyTaxe = calculateCost(nb_actions,a[2][i]) - buyPrice
                        else:
                            time += 1
                            fraction = (a[2][i]-buyUniquePrice)/a[2][i]
                            if  fraction >= maxFracTMP or fraction <= minFracTMP :
                                trade = False
                                time = 0
                                sellPrice = nb_actions*a[2][i]
                                sellTaxe = calculateCost(nb_actions,a[2][i]) - sellPrice
                                totalTMP.append( sellPrice - buyPrice - buyTaxe - sellTaxe )
                                buyPrice = None
                        i += 1
                    if trade:
                        trade = False
                        sellPrice = nb_actions*a[2][i]
                        sellTaxe = calculateCost(nb_actions,a[2][i]) - sellPrice
                        totalTMP.append( sellPrice - buyPrice - buyTaxe - sellTaxe )
                        buyPrice = None
                    total[j][k][l][m] += totalTMP
    return total

def calculateCost(nb_shares,price_per_share):
    flat_rate = 0.005*nb_shares #Flat Rate
    if flat_rate < 1:
        flat_rate = 1
    costTot = flat_rate
    
    cost_plus = 0.0035*nb_shares #Cost Plus
    costTot += cost_plus
    
    costTot += price_per_share * nb_shares #Transaction Cost
    return costTot
#####################################################################################
# MAIN ALGORITHM
def main(min_,max_):
    mypath = getcwd()+"/../Results/treeAnalysisCurves/"
    onlyfiles = listdir(mypath)
    # 
    n = 10
#     lenlen = len(onlyfiles)
    pool = Pool()
    var = arange(min_, max_, 20).tolist()
    if max_ not in var:
        var.append(max_)
    #
    for i in xrange(len(var)-1):
        total = createBigList(n)
        nbTransaction = createBigList(n)
        print var[i], var[i+1]
        pourcentPerCompany = pool.map(functools.partial(calculate, list_=onlyfiles), range(var[i], var[i+1]))
          
        for pourcent in pourcentPerCompany:
            for j in xrange(n):
                for k in xrange(n):
                    for l in xrange(n):
                        for m in xrange(n):
                            total[j][k][l][m] += pourcent[j][k][l][m]
        # action  
        nbTransaction, total = listNbTransactionTotalToNumerical(n, nbTransaction, total)
        # write
        writePickle(total,"../total/total"+str(var[i]))     
        writePickle(nbTransaction,"nbTransaction"+str(var[i]))

######################################################################################
# MERGE ALGORITHMS
def mergeTotal():
    n = 10
    total = createBigList(n)
    mypath = getcwd()+"/result/total/"
    onlyfiles = listdir(mypath)
    for file_ in onlyfiles:
        if file_ != ".DS_Store":
            print onlyfiles.index(file_), len(onlyfiles)
            totalTMP = readPickle("../total/"+file_)
            for j in xrange(n):
                    for k in xrange(n):
                        for l in xrange(n):
                            for m in xrange(n):
                                total[j][k][l][m] += [totalTMP[j][k][l][m]]
    return total

def mergeNbTransactions():
    n = 10
    nbTransaction = createBigList(n)
    mypath = getcwd()+"/result/data/"
    onlyfiles = listdir(mypath)
    for file_ in onlyfiles:
        if file_ != ".DS_Store":
            print onlyfiles.index(file_), len(onlyfiles)
            nbTransactionTMP = readPickle(file_)
            for j in xrange(n):
                    for k in xrange(n):
                        for l in xrange(n):
                            for m in xrange(n):
                                nbTransaction[j][k][l][m] += [nbTransactionTMP[j][k][l][m]]
    return nbTransaction
    
######################################################################################
# REARRAGE AND SAVE
def sumAndSave(total, nbTransaction):
    total = listToNumerical(10,total)
    nbTransaction = listToNumerical(10,nbTransaction)
    writePickle(total,"../totalTotal/total")
    writePickle(nbTransaction,"../totalTotal/nbTransaction")
    return total, nbTransaction
    
def totalNbTransactionNormaliseAndSave(total, nbTransaction):
    n = 10
    totalNbTransaction = createBigList(n)
    for j in xrange(n):
        for k in xrange(n):
            for l in xrange(n):
                for m in xrange(n):
                    totalNbTransaction[j][k][l][m] = total[j][k][l][m] / nbTransaction[j][k][l][m]
        writePickle(totalNbTransaction,"../totalTotal/totalNormalised")
    return totalNbTransaction

######################################################################################
# 3D PLOT
def paint(total):
    n = 10
    for i in xrange(n):
        for j in xrange(n):
            plot3D( total[i][j], "maxOutput"+str(i)+"maxDerive"+str(j), n)

######################################################################################
# GET FINAL ANSWER
def getMaxGain(total,nbTransaction):
    n = 10
    gainTMP = total[0][0][0][0]
    a,b,c,d = 0,0,0,0
    maxOutput = linspace(0.1, 1, 10)
    maxDerive = linspace(0.1, 1, 10)
    maxFrac = linspace(0.0005, 0.04, 10)
    minFrac = linspace(-0.0005, -0.04, 10)
    for j in xrange(n):
        for k in xrange(n):
            for l in xrange(n):
                for m in xrange(n):
                    if total[j][k][l][m] > gainTMP:
                        a,b,c,d = j,k,l,m
                        gainTMP = total[a][b][c][d]
                        print a,b,c,d, gainTMP, nbTransaction[a][b][c][d]
    print "gainMAX :"+str(gainTMP)
    print "parametres optimaux : "+str(maxOutput[a])+", "+str(maxDerive[b])+", "+str(maxFrac[c])+", "+str(minFrac[d])
    print "a,b,c,d : ",a,b,c,d
    print "nbTransaction : ", nbTransaction[a][b][c][d]
    
###################################################################################### 
def getListeSommation(list_):
    tmpList = []
    tmpList.append(list_[0])
    for i in xrange(1,len(list_)):
        tmpList.append(tmpList[i-1] + list_[i])
    return tmpList

###################################################################################### 
# mypath = getcwd()+"/../Results/treeAnalysisCurves/"
# onlyfiles = listdir(mypath)
# main(1,len(onlyfiles))

# print "Merging TOTAL"
# total = mergeTotal()
# print "Merging NBTRANSACTION"
# nbTransaction = mergeNbTransactions()
# total, nbTransaction = sumAndSave(total,nbTransaction) 

# print "start - 1"
# total = readPickle("../totalTotal/total")
# nbTransaction = readPickle("../totalTotal/nbTransaction")
# totalNormalized = totalNbTransactionNormaliseAndSave(total,nbTransaction)

# print "start - 2"
# paint(totalNormalized)
# getMaxGain(totalNormalized,nbTransaction)
# print "END"

total = mergeTotal()
totalBest = getListeSommation(total[0][9][8][6])
simplePlot(totalBest)


