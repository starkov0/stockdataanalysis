import csv
from os import listdir, getcwd
from numpy import sum, mean, max, array, meshgrid, ones
import matplotlib.pyplot as plt
import time
import pickle

#####################################################################################
def readCSV(name):
    data = []
    resultFile = open(name,'rb')
    rd = csv.reader(resultFile, dialect='excel')
    for row in rd:
        data.append(row)
    return data

def showBestConfiguration(z,n,title):
    fig, ax = plt.subplots()
    phi_m = range(0,n)
    phi_p = range(0,n)
    x,y = meshgrid(phi_p, phi_m)
    p = ax.pcolor(x, y, z)
    cb = fig.colorbar(p, ax=ax)
    plt.title(title)
    plt.xlabel("maxOutput")
    plt.ylabel("maxDerive")
    plt.savefig("./result/img/"+title)
    
def noramlize(matrix_, signe):
    meanMatrix = sum(sum(matrix_))
    for j in xrange(len(matrix_)):
        for k in xrange(len(matrix_)): 
            matrix_[j][k] = signe*matrix_[j][k]/meanMatrix
    return matrix_

def writePickle(data,name):
    pickle.dump( data, open( "./result/data/"+name, "wb" ) )

def readPickle(name):
    return pickle.load(open( "./result/data/"+name, "rb" ))

    
def createList():
    list1 = [(array(range(1,n+1), dtype = float)/n).tolist() for i in xrange(n)]
    list2 = [((ones(n)*(i+1))/n).tolist() for i in xrange(n)]
    c = []
    for i in xrange(len(list1)):
        for j in xrange(len(list1[0])):
            c.append([list1[i][j],list2[i][j]])
    return c

def listToNumerical(n, nbTransaction, pourcentGain, pourcentPerte):
    for j in xrange(n):
        for k in xrange(n):
            nbTransaction[j][k] = len(pourcentGain[j][k])
            if pourcentGain[j][k] == []:
                pourcentGain[j][k] = 0
            else:
                pourcentGain[j][k] = mean(pourcentGain[j][k])
            if pourcentPerte[j][k] == []:
                pourcentPerte[j][k] = 0
            else:
                pourcentPerte[j][k] = mean(pourcentPerte[j][k])
    return nbTransaction, pourcentGain, pourcentPerte
    
def calculate(file_, list_):
    a = readCSV(getcwd()+"/../Results/treeAnalysisCurves/"+list_[file_])
    a[2] =  list(array(a[2], dtype = float))
    a[4] =  list(array(a[4], dtype = float))
    a[6] =  list(array(a[6], dtype = float))
    pourcentGain = [[[] for j in xrange(n)] for i in xrange(n)]
    pourcentPerte = [[[] for j in xrange(n)] for i in xrange(n)]
    maxOutput = list(float(u)/n for u in range(1, 1+n))
    maxDerive = list(float(u)/n for u in range(1, 1+n))
    for j in xrange(n):
        for k in xrange(n):
            maxTMP = maxOutput[j]
            maxDeriveTMP = maxDerive[k]
            pourcentGainTMP = []
            pourcentPerteTMP = []
            lenList = len(a[2])
            i = 0
            while i < lenList-1:
                if a[4][i] > maxTMP and a[6][i] > maxDeriveTMP: 
                    if 60+i > lenList:
                        max_ = max(a[2][i+1:lenList])
                        maxIndex = a[2][i+1:lenList].index(max_)
                        min_ = min(a[2][i+1:lenList])
                        minIndex = a[2][i+1:lenList].index(min_)
                        pourcentGainTMP.append( (max_-a[2][i])/a[2][i] )
                        pourcentPerteTMP.append( (min_-a[2][i])/a[2][i] )
                        i += 1+min(maxIndex, minIndex)
                    else:
                        max_ = max(a[2][i+1:i+60])
                        maxIndex = a[2][i+1:i+60].index(max_)
                        min_ = min(a[2][i+1:i+60])
                        minIndex = a[2][i+1:i+60].index(min_)
                        pourcentGainTMP.append( (max_-a[2][i])/a[2][i] )
                        pourcentPerteTMP.append( (min_-a[2][i])/a[2][i] )
                        i += 1+min(maxIndex, minIndex)
                else:
                    i += 1
            pourcentGain[j][k] += pourcentGainTMP
            pourcentPerte[j][k] += pourcentPerteTMP
    return pourcentGain, pourcentPerte

#####################################################################################
mypath = getcwd()+"/../Results/treeAnalysisCurves/"
onlyfiles = listdir(mypath)
 
 
n = 100
maxFrac = list(float(a)/(n*10) for a in range(1, 1+n))
minFrac = list(float(-a)/(n*10) for a in range(1, 1+n))
pourcentGain = [[[] for j in xrange(n)] for i in xrange(n)]
nbTransaction = [[[] for j in xrange(n)] for i in xrange(n)]
lenlen = len(onlyfiles)

maxOutput = 0.5
maxDerive = 0.8

for fileNb in xrange(5,50):
    print fileNb
    a = readCSV(getcwd()+"/../Results/treeAnalysisCurves/"+onlyfiles[fileNb])
    lenList = len(a[2])
    a[2] =  list(array(a[2], dtype = float))
    a[4] =  list(array(a[4], dtype = float))
    a[6] =  list(array(a[6], dtype = float))
    for j in xrange(n):
        for k in xrange(n):
            maxFracTMP = maxFrac[j]
            minFracTMP = minFrac[k]
            pourcentGainTMP = []
            #
            trade = False
            buyPrice = None
            time = 0
            i = 0
            while i < lenList-1:
                if not trade:
                    if a[4][i] > maxOutput and a[6][i] > maxDerive: 
                        trade = True
                        buyPrice = a[2][i]
                        print "BUYPRICE :", buyPrice
                if trade:
                    time += 1
                    fraction = (a[2][i]-buyPrice)/a[2][i]
                    if  fraction >= maxFracTMP or fraction <= minFracTMP :
                        trade = False
                        time = 0
                        pourcentGainTMP.append( (a[2][i]-buyPrice)/buyPrice )
                        print "SELL PRICE :", a[2][i]
                        print "TOTAL GAIN : !!!!!!!! : ", (a[2][i]-buyPrice)/buyPrice
                i += 1
            if trade: 
                trade = False
                pourcentGainTMP.append( (a[2][i]-buyPrice)/buyPrice )
                print "SELL PRICE :", a[2][i]
                print "TOTAL GAIN : !!!!!!!! : ", (a[2][i]-buyPrice)/buyPrice
            pourcentGain[j][k] += pourcentGainTMP
  
for j in xrange(n):
    for k in xrange(n): 
        nbTransaction[j][k] = len(pourcentGain[j][k])
        pourcentGain[j][k] = mean(pourcentGain[j][k])
 
 


mypath = getcwd()+"/../Results/treeAnalysisCurves/"
onlyfiles = listdir(mypath)
# 
n = 10
maxOutput = list(float(a)/n for a in range(1, 1+n))
maxDerive = list(float(a)/n for a in range(1, 1+n))
pourcentGain = [[[] for j in xrange(n)] for i in xrange(n)]
pourcentPerte = [[[] for j in xrange(n)] for i in xrange(n)]
nbTransaction = [[[] for j in xrange(n)] for i in xrange(n)]
bestChoice = [[0 for j in xrange(n)] for i in xrange(n)]
lenlen = len(onlyfiles)
######################################################################################
# pool = Pool()
# listTot = createList()
# pourcentPerCompany = pool.map(functools.partial(calculate, list_=onlyfiles), range(1,lenlen))
# #
# for pourcent in pourcentPerCompany:
#     for j in xrange(n):
#         for k in xrange(n):
#             pourcentGain[j][k] += pourcent[0][j][k]
#             pourcentPerte[j][k] += pourcent[1][j][k]
# # action  
# nbTransaction, pourcentGain, pourcentPerte = listToNumerical(n, nbTransaction, pourcentGain, pourcentPerte)
    