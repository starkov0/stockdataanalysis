# coding=utf-8
from dataObject import *
import numpy as np
import pybrain
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
import matplotlib.pyplot as plt
import cPickle as pickle
from scipy.signal import argrelextrema
from math import pi, sqrt, exp
from scipy import *



##################################################################################
# data varaibles
past_time = 10
futur_time = 20
nb_signals = 8

# neural network arguments
input_ = past_time * nb_signals
output = 1
hidden = input_

# data variable
startAnalysisValue = 90

# number of weeks of train
nbTrainWeeks = 4
trainCompanies_Start = 0
trainCompanies_End = 3
testCompany = 2
testWeek = 4

# net file name
net_File = "./neuralNetwork"
##################################################################################


class analysisObject():

	def __init__(self):
		self.dataObj = dataObject(past_time,futur_time)
		self.week = 0
		self.companyNumber = 0
		self.loadIndexes()


##################################################################################
# data -> change
	# returns the sign of the value X
	def sign(self,x): 
		returnValue = 1 if x >= 0 else -1
		return returnValue

	# takes a vector and applies a threshold to the vector -> upper
	def getUpperThan(self,data,limit):
		returnData = np.zeros([1,len(data)],dtype = float)[0]
		for i in xrange(len(data)):
			if data[i] < limit: returnData[i] = limit
			else: returnData[i] = data[i]
		return returnData

	# takes a vector and applies a threshold to the vector -> lower
	def getLowerThan(self,data,limit):
		returnData = np.zeros([1,len(data)],dtype = float)[0]
		for i in xrange(len(data)):
			if data[i] > limit: returnData[i] = limit
			else: returnData[i] = data[i]
		return returnData

	# deletes NaN values in a vector
	def deleteNanValues(self,vect):
		return vect[np.invert(np.isnan(vect))]

	# creates a gaussian vector
	def gauss(n=10,sigma=1):
		r = [-10,-9,-8,-7,-6,-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5,6,7,8,9,10]
		# r = range(-int(n/2),int(n/2)+1)
		return [1 / (sigma * sqrt(2*pi)) * exp(-float(x)**2/(2*sigma**2)) for x in r]


##################################################################################
# data object -> communicate with
	# allows to load the data of the next comapany
	def loadCompanyData(self):
		self.dataObj.loadCompanyData(self.companyNumber)

	def loadIndexes(self):
		self.dataObj.loadIndexData()

	def prepareDataForClassifcation(self):
		self.week = testWeek
		self.companyNumber = testCompany
		self.loadCompanyData()
##################################################################################
# neural network -> create network
	# initiates the neural network (NN)
	def initiateNeuralNetwork(self):
		self.net = buildNetwork(input_, hidden, output)
		self.net.randomize()
		self.ds = SupervisedDataSet(input_, output)

	# creates a new neural network or loads it from a file
	def getNeuralNetwork(self):
		try: #if neural network exists -> lets load it!
			with open(net_File):
				self.loadNeuralNetwork()

		except IOError: # neural network does not exist -> lets create it!
			self.initiateNeuralNetwork()
			for j in xrange(trainCompanies_Start,trainCompanies_End):
				self.companyNumber = j
				self.loadCompanyData()
				for i in xrange(nbTrainWeeks):
					self.week = i
					self.learnWeekNeuralNetwork(i)

			anal.trainNeuralNetwork()
			anal.saveNeuralNetwork()

			# add sample to neural network
	def learnNeuralNetwork(self,inputSample,outputSample):
		self.ds.addSample(inputSample,outputSample)

	# learn data per week and put it into the neural network (NN)
	def learnWeekNeuralNetwork(self,week):
		length = len(self.dataObj.weekData[week][0])-futur_time
		for time in xrange(startAnalysisValue,length):
			input_ = self.getInput(week,time)
			output = self.getOutput(week,time)
			self.learnNeuralNetwork(input_, output)

	def printErreur(self,error):
		print "Neural Network error while training : "+str(error)


	# train neural network until convergence - TODO
	def trainNeuralNetwork(self):
		self.trainer = BackpropTrainer(self.net, self.ds)                                                     
		for epoch in xrange(0, 100):
			error = self.trainer.train()
			self.printErreur(error)
			if error < 0.65:
				break

# saves the neural network into a file
	def saveNeuralNetwork(self):
		fileObject = open(net_File, 'w')
		pickle.dump(self.net, fileObject)
		fileObject.close()

	# loads the neural network from a file
	def loadNeuralNetwork(self):
		fileObject = open(net_File, 'r')
		self.net = pickle.load(fileObject)
		fileObject.close()

##################################################################################
# neural network -> prepare data
	# gets inputs as calculation of the different signals for the neural network (NN)
	def getInput(self,week,time):
		adx = self.deleteNanValues(self.dataObj.getADX(week,time))
		sma = self.deleteNanValues(self.dataObj.getSMA(week,time))
		t3 = self.deleteNanValues(self.dataObj.getT3(week,time))
		sar = self.deleteNanValues(self.dataObj.getSAR(week,time))
		sarext = self.deleteNanValues(self.dataObj.getSAREXT(week,time))
		atr = self.deleteNanValues(self.dataObj.getATR(week,time))
		rsi =  self.deleteNanValues(anal.dataObj.getRSI(week,time))
		roc =  self.deleteNanValues(anal.dataObj.getROC(week,time))
		# pastHighLowMean = anal.dataObj.getPastHighLowMeanPrice(week,time)
		# pastVolume =  anal.dataObj.getPastVolume(week,time)
		# pastHigh =  anal.dataObj.getPastHigh(week,time)
		# pastDOW =  anal.dataObj.getPastDOW(week,time)
		# pastSANDP500 =  anal.dataObj.getPastSANDP500(week,time)
		# ,pastDOW,pastSANDP500,pastHighLowMean,pastVolume,pastHigh
		return (np.concatenate([adx,sma,t3,sar,sarext,atr,rsi,roc])*100).tolist()

	# gets outputs for the neural network (NN)
	def getOutput(self,week,time):
		# min_ = self.dataObj.getNextMinValue(week,time) * 10000		
		# max_ = self.dataObj.getNextMaxValue(week,time) * 10000
		# return [min_,max_]
		# futurMaxPrice = anal.dataObj.getFuturMaxPrice(week,time)* 100
		riskMin = anal.dataObj.getFuturMinRisk(week,time)
		return [riskMin]

##################################################################################
# neural network -> test
	# classify a data with the neural network
	def testNeuralNetwork(self,input_):
		return self.net.activate(input_)
	
	# classify week data
	def testWeekNeuralNetwork(self,week):
		length = len(self.dataObj.weekData[week][0])-futur_time
		real = np.zeros([1,length-startAnalysisValue], dtype = float)[0]
		classified = np.zeros([1,length-startAnalysisValue], dtype = float)[0]
		for time in xrange(startAnalysisValue,length):
			real[time-startAnalysisValue] = self.getOutput(week,time)[0]
			classified[time-startAnalysisValue] = self.testNeuralNetwork(anal.getInput(week,time))[0]
		return real, classified
		# real = np.zeros([2,length-startAnalysisValue], dtype = float)
		# classified = np.zeros([2,length-startAnalysisValue], dtype = float)
		# for time in xrange(startAnalysisValue,length):
		# 	r = self.getOutput(week,time)
		# 	c = self.classifyNeuralNetwork(anal.getInput(week,time))

		# 	real[0][time-startAnalysisValue] = r[0]
		# 	real[1][time-startAnalysisValue] = r[1]
		# 	classified[0][time-startAnalysisValue] = c[0]
		# 	classified[1][time-startAnalysisValue] = c[1]

##################################################################################
# neural network -> analyse data
	# calculates classification error
	def getErrorNeuralNetwork(self,real,classified):
		# real = self.getUpperThan(real,0)
		# classified = self.getUpperThan(classified,0)
		error = 0.
		tot = 0.
		for i in xrange(10,len(real)):
			if classified[i] != 0:
				tot += 1
				if self.sign(real[i] - real[i-1]) != self.sign(classified[i] - classified[i-1]):
					error += 1
		return error/tot
		
		# realMin = self.getLowerThan(realOutput[0],0)
		# realMax = self.getUpperThan(realOutput[1],0)
		# classifiedMin = self.getLowerThan(classifiedOutput[0],0)
		# classifiedMax = self.getUpperThan(classifiedOutput[1],0)

		# realMinMean = np.mean(realMin)
		# realMaxMean = np.mean(realMax)
		# classifiedMinMean = np.mean(classifiedMin)
		# classifiedMaxMean = np.mean(classifiedMax)

		# for i in xrange(1,length):
		# 	if self.sign(realMin[i] - realMinMean) != self.sign(classifiedMin[i] - classifiedMinMean): errorMin = errorMin + 1
		# 	if self.sign(realMax[i] - realMaxMean) != self.sign(classifiedMax[i] - classifiedMaxMean): errorMax = errorMax + 1

		# return realOutput, classifiedOutput, [errorMin/length, errorMax/length]

	# plot data in different ways
	def plotOutputs(self,realOutput,classifiedOutput):
		time = 600

		realTmpMin = self.getLowerThan(realOutput[0][0:time],0.)
		realTmpMax = self.getUpperThan(realOutput[1][0:time],0.)
		classifiedTmpMin = self.getLowerThan(classifiedOutput[0][0:time],0.)
		classifiedTmpMax = self.getUpperThan(classifiedOutput[1][0:time],0.0015)

		# local Maxima
		realExtremaData = argrelextrema(realTmpMax, np.greater)[0]
		classifiedExtremaData = argrelextrema(classifiedTmpMax, np.greater)[0]

		realExtrema = np.zeros([1,time], dtype = float)[0]
		classifiedExtrema = np.zeros([1,time], dtype = float)[0]

		print realExtremaData
		print classifiedExtremaData

		for i in realExtremaData:
			realExtrema[i] = realOutput[1][i]
		for i in classifiedExtremaData:
			classifiedExtrema[i] = classifiedOutput[1][i]

		classifiedTmpMax2 = classifiedTmpMax*10
		classifiedTmpMax2 = classifiedTmpMax2 - min(classifiedTmpMax2)

		# first figure
		fig = plt.figure()
		ax1 = fig.add_subplot(311)
		ax2 = fig.add_subplot(312)
		ax3 = fig.add_subplot(313)

		# ax1.plot(abs(realTmpMin), color = 'r')
		ax1.plot(realOutput[1], color = 'b')
		# ax2.plot(abs(classifiedTmpMin), color = 'r')
		ax1.plot(classifiedOutput[1], color = 'r')
		ax2.plot(classifiedTmpMax, color = 'b')
		ax3.plot(self.dataObj.weekData[testWeek][5][0:time])
		plt.show()

		fig1 = plt.figure()
		ax4 = fig1.add_subplot(111)
		ax4.plot(realExtrema, color = 'g')
		ax4.plot(classifiedExtrema, color = 'y')

		plt.show()



##################################################################################
# create analysis Object
anal = analysisObject()

# get a neural Network
anal.getNeuralNetwork()
anal.prepareDataForClassifcation()

real, classified = anal.testWeekNeuralNetwork(testWeek)

real2 = anal.getLowerThan(real,0)
classified2 = anal.getLowerThan(classified,0)

median = np.median(classified2)
classified3 = classified2-median

classified4 = (anal.getUpperThan(classified3,0))
classified5 = abs(classified3)

fig = plt.figure()
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312)
ax3 = fig.add_subplot(313)
ax1.plot(real, color = 'k')
ax1.plot(classified, color = 'y')
ax2.plot(classified, color = 'b')
ax3.plot(anal.dataObj.weekData[testWeek][3])
# ax3.plot(classified5, color = 'g')
plt.show()

# print anal.getErrorNeuralNetwork(real,classified4)



print anal.dataObj.symbol



























# minC = 0
# minP = 100000

# for i in xrange(len(anal.dataObj.listSymbols)):
# 	anal.companyNumber = i
# 	anal.loadData()
# 	anal.dataObj.separatePricePerWeek()
# 	if anal.dataObj.weekData[0][5][0] < minP:
# 		minP = anal.dataObj.weekData[0][5][0]
# 		minC = i

# print minP
# print minC



# realExtrema = np.zeros([1,len(real)], dtype = float)[0]
# classifiedExtrema = np.zeros([1,len(real)], dtype = float)[0]

# realExtremaData = argrelextrema(real, np.greater)[0]
# classifiedExtremaData = argrelextrema(classified2, np.greater)[0]

# for i in realExtremaData:
# 	realExtrema[i] = real[i]
# for i in classifiedExtremaData:
# 	classifiedExtrema[i] = classified2[i]


# fig = plt.figure()
# ax1 = fig.add_subplot(211)
# ax2 = fig.add_subplot(212)
# ax1.plot(realExtrema, color = 'k')
# ax1.plot(classifiedExtrema, color = 'y')
# ax2.plot(classifiedExtrema, color = 'b')
# plt.show()


# # classify second week by what has been learned the first week
# realOutput, classifiedOutput, error = anal.classifyAndGetErrorNeuralNetwork(nbTrainWeeks)
# print "Classification Error : " + str(error)

# anal.plotOutputs(realOutput, classifiedOutput)






