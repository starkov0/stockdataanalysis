# coding=utf-8
import talib
from numpy import array, concatenate
from Data import Data

##################################################################################
# variables used by signals functions
date = 0
time = 1
open_ = 2
high = 3
low = 4
close = 5
volume = 6
##################################################################################

class Indicator(Data):

	def __init__(self,varObj):
		super(Indicator, self).__init__(varObj)


##################################################################################
	def continueToDiscret(self,up,middle,low,data):
		if data > up:
			data = 3
		elif data > middle:
			data = 2
		elif data > low:
			data = 1
		else:
			data = -1
		return data
##################################################################################
# get indicators -> FUTUR
	def getNextMaxValue(self,time):
		day = self.day
		length = len(self.dayData[day][high])
# 		if length - time > 40:
# 			length = time + 40
		maxValue = max(self.dayData[day][close][0:length])
		timeValue = self.dayData[day][close][time]
		result = (maxValue-timeValue)
# 		result = (maxValue-timeValue)#/timeValue
#  		result = self.continueToDiscret(0.16, 0.16, 0.16, result)
		return result

	def getNextMinValue(self,time):
		day = self.day
		length = len(self.dayData[day][high])
# 		if length - time > 40:
# 			length = time + 40
		minValue = min(self.dayData[day][close][0:length])
		timeValue = self.dayData[day][close][time]
		result = (minValue-timeValue)
# 		result = (timeValue-minValue)#/timeValue
# 		result = self.continueToDiscret(0.15, 0.15, 0.15, result)
		return 1
	
# 		maxValue = max(self.dayData[day][close][time+1:length])
#  		result = (self.dayData[day][close][time+1:length].index(maxValue))

	def getPrice(self,time):
		day = self.day
		return_ = self.dayData[day][close][time]
		return return_

##################################################################################
# indicators -> SBN
	def getMACD(self,time):	
		day = self.day
		macd, macdsignal, macdhist = talib.MACD( array( self.dayData[day][close][time-(33+3):time], dtype = float ) )
		return self.deleteNanValues(macd), self.deleteNanValues(macdsignal)


	def getEMAV(self,time):	
		day = self.day
		emav = talib.EMA( array( self.dayData[day][volume][time-(29+1):time], dtype = float ) ) 
		return self.deleteNanValues(emav)

	def getEMAP(self,time):	
		day = self.day 
		emap = talib.EMA( array( self.dayData[day][close][time-(29+1):time], dtype = float ) ) 
		return self.deleteNanValues( array(emap) )

	def getSMAK(self,time):	
		day = self.day
		slowk, slowd = talib.STOCH( array( self.dayData[day][high][time-(8+29+3):time], dtype = float ),
		array( self.dayData[day][low][time-(8+29+3):time], dtype = float ),
		array( self.dayData[day][close][time-(8+29+3):time], dtype = float ) ) 
		slowk = self.deleteNanValues(slowk)
		smak = talib.SMA( slowk ) 
		return self.deleteNanValues(smak)

	def getSTOCH(self,time):	
		day = self.day
		slowk, slowd = talib.STOCH( array( self.dayData[day][high][time-(8+3):time], dtype = float ),
		array( self.dayData[day][low][time-(8+3):time], dtype = float ),
		array( self.dayData[day][close][time-(8+3):time], dtype = float ) ) 
		return self.deleteNanValues(slowk)

	def getBBANDS(self,time):	
		day = self.day
		upper, middle, lower = talib.BBANDS( array( self.dayData[day][close][time-(19+1):time], dtype = float ), timeperiod = 20 )
		return self.deleteNanValues(upper), self.deleteNanValues(middle), self.deleteNanValues(lower)

	def getPRICE(self,time):	
		day = self.day
		price = self.dayData[day][close][time]
		return [price]

	def getVOLUME(self,time):	
		day = self.day
		volume_ = float(self.dayData[day][volume][time])
		return [volume_]

##################################################################################
# past_time -> Overlap Studies Functions
	def getDerivePremiere(self,time):	
		day = self.day
		timeT = self.dayData[day][close][time]
		timeT1 = self.dayData[day][close][time-1]
		derive = timeT - timeT1		
		return [derive]

	def getDeriveSeconde(self,time):	
		day = self.day
		timeT = self.dayData[day][close][time]
		timeT1 = self.dayData[day][close][time-1]
		timeT2 = self.dayData[day][close][time-2]

		derive1 = timeT - timeT1
		derive2 = timeT1 - timeT2
		deriveSec = derive1 - derive2
		return [deriveSec]

	def getDEMApasttime(self,time):	
		day = self.day
		dema = talib.DEMA( array( self.dayData[day][volume][time-(58+1): time], dtype = float ) ) 
		return self.deleteNanValues(dema)
		
	def getEMAVpasttime(self,time):	 
		day = self.day
		emav = talib.EMA( array( self.dayData[day][volume][time-(29+1): time], dtype = float ) ) 
		return self.deleteNanValues(emav)

	def getEMAPpasttime(self,time):	 
		day = self.day
		emap = talib.EMA( array( self.dayData[day][close][time-(29+1): time], dtype = float ) ) 
		return self.deleteNanValues( array(emap) )

	def getHT_TRENDLINEpasttime(self,time):	 
		day = self.day
		ht = talib.HT_TRENDLINE( array( self.dayData[day][close][time-(63+1): time], dtype = float ) ) 
		return self.deleteNanValues( array(ht) )	

	def getMApasttime(self,time):	
		day = self.day 
		ma = talib.MA( array( self.dayData[day][close][time-(30):time], dtype = float ) ) 
		return self.deleteNanValues( array(ma) )	

	def getMIDPOINTpasttime(self,time):	 
		day = self.day
		real = talib.MIDPOINT( array( self.dayData[day][close][time-(13+1): time], dtype = float ) ) 
		return self.deleteNanValues( array(real) )	

	def getMIDPRICEpasttime(self,time):	 
		day = self.day
		real = talib.MIDPRICE( array( self.dayData[day][high][time-(13+1): time], dtype = float ),
			array( self.dayData[day][low][time-(40):time], dtype = float ) ) 
		return self.deleteNanValues( array(real) )	

	def getSARpasttime(self,time):	 
		day = self.day
		sar = talib.SAR( array( self.dayData[day][high][time-(1+1): time] ),
		array( self.dayData[day][low][time-1:time] ) ) 
		return self.deleteNanValues(sar)

	def getSAREXTpasttime(self,time):	 
		day = self.day
		sar = talib.SAREXT( array( self.dayData[day][high][time-(1+1): time] ),
		array( self.dayData[day][low][time-1:time] ) ) 
		return self.deleteNanValues(sar)

	def getT3pasttime(self,time):	 
		day = self.day
		t3 = talib.T3( array( self.dayData[day][close][time-(24+1): time] ) )
		return self.deleteNanValues(t3)

	def getTEMApasttime(self,time):	 
		day = self.day
		real = talib.T3( array( self.dayData[day][close][time-(24+1): time] ) )
		return self.deleteNanValues(real)

	def getTRIMApasttime(self,time):	 
		day = self.day
		real = talib.TRIMA( array( self.dayData[day][close][time-(29+1): time] ) )
		return self.deleteNanValues(real)

	def getWMApasttime(self,time):	 
		day = self.day
		real = talib.WMA( array( self.dayData[day][close][time-(29+1): time] ) )
		return self.deleteNanValues(real)

##################################################################################
# past_time -> Momentum Indicator Functions
	def getADXpasttime(self,time):	 
		day = self.day
		adx = talib.ADX( array( self.dayData[day][high][time-(27+1): time], dtype = float ),
			array( self.dayData[day][low][time-(27+1): time], dtype = float ),
			array( self.dayData[day][close][time-(27+1): time], dtype = float ) )
		return self.deleteNanValues(adx)

	def getADXRpasttime(self,time):	 
		day = self.day
		real = talib.ADXR( array( self.dayData[day][high][time-(40+1): time], dtype = float ),
			array( self.dayData[day][low][time-(40+1): time], dtype = float ),
			array( self.dayData[day][close][time-(40+1): time], dtype = float ) )
		return self.deleteNanValues(real)

	def getAPOpasttime(self,time):	
		day = self.day
		real = talib.APO( array( self.dayData[day][close][time-(25+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getAROONOSCpasttime(self,time):	
		day = self.day
		real = talib.AROONOSC( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getBOPpasttime(self,time):	
		day = self.day
		real = talib.BOP( array( self.dayData[day][open_][time-(0+1): time], dtype = float ),
			array( self.dayData[day][high][time-(0+1): time], dtype = float ),
			array( self.dayData[day][low][time-(0+1): time], dtype = float ),
			array( self.dayData[day][close][time-(0+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getCCIpasttime(self,time):	
		day = self.day
		real = talib.CCI( array( self.dayData[day][high][time-(13+1): time], dtype = float ),
			array( self.dayData[day][low][time-(13+1): time], dtype = float ),
			array( self.dayData[day][close][time-(13+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getCMOpasttime(self,time):	
		day = self.day
		real = talib.CMO( array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getDXpasttime(self,time):	
		day = self.day
		real = talib.DX( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getMACDpasttime(self,time):	
		day = self.day
		macd, macdsignal, macdhist = talib.MACD( array( self.dayData[day][close][time-(33+1): time], dtype = float ) ) 
		return self.deleteNanValues(macdhist)


	def getMFIpasttime(self,time):	
		day = self.day
		real = talib.MFI( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ),
			array( self.dayData[day][volume][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getMINUS_DIpasttime(self,time):	
		day = self.day
		real = talib.MINUS_DI( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getMINUS_DMpasttime(self,time):	
		day = self.day
		real = talib.MINUS_DM( array( self.dayData[day][high][time-(13+1): time], dtype = float ),
			array( self.dayData[day][low][time-(13+1): time], dtype = float ) )
		return self.deleteNanValues(real)

	def getMOMpasttime(self,time):	
		day = self.day
		real = talib.MOM( array( self.dayData[day][close][time-(10+1): time], dtype = float ) )
		return self.deleteNanValues(real)

	def getPLUS_DIpasttime(self,time):	
		day = self.day
		real = talib.PLUS_DI( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getPLUS_DMpasttime(self,time):	
		day = self.day
		real = talib.PLUS_DM( array( self.dayData[day][high][time-(13+1): time], dtype = float ),
			array( self.dayData[day][low][time-(13+1): time], dtype = float ) )
		return self.deleteNanValues(real)

	def getPPOpasttime(self,time):	
		day = self.day
		real = talib.PPO( array( self.dayData[day][close][time-(25+1): time], dtype = float ) )
		return self.deleteNanValues(real)

	def getROCpasttime(self,time):	 
		day = self.day
		real = talib.ROC( array( self.dayData[day][close][time-(10+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getROCPpasttime(self,time):	 
		day = self.day
		real = talib.ROCP( array( self.dayData[day][close][time-(10+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getROCRpasttime(self,time):	 
		day = self.day
		real = talib.ROCR( array( self.dayData[day][close][time-(10+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getROCR100pasttime(self,time):	 
		day = self.day
		real = talib.ROCR100( array( self.dayData[day][close][time-(10+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getRSIpasttime(self,time):	 
		day = self.day
		real = talib.RSI( array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	# 8 = NAN de stoch / 3 = number of values needed
	def getSTOCHpasttime(self,time):	
		day = self.day
		slowk, slowd = talib.STOCH( array( self.dayData[day][high][time-(8+1): time], dtype = float ),
		array( self.dayData[day][low][time-(8+1): time], dtype = float ),
		array( self.dayData[day][close][time-(8+1): time], dtype = float ) ) 
		return concatenate([self.deleteNanValues(slowk), self.deleteNanValues(slowk)])

	# 8 = NAN de STOCH / 29 = NAN de SMA / 3 = number of values needed
	def getSMAKpasttime(self,time):	
		day = self.day
		slowk, slowd = talib.STOCH( array( self.dayData[day][high][time-(8+29+1): time], dtype = float ),
		array( self.dayData[day][low][time-(8+29+1): time], dtype = float ),
		array( self.dayData[day][close][time-(8+29+1): time], dtype = float ) ) 
		slowk = self.deleteNanValues(slowk)
		smak = talib.SMA( slowk ) 
		return self.deleteNanValues(smak)

	def getTRIXpasttime(self,time):	 
		day = self.day
		real = talib.TRIX( array( self.dayData[day][close][time-(88+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getULTOSCpasttime(self,time):	 
		day = self.day
		real = talib.ULTOSC( array( self.dayData[day][high][time-(28+1): time], dtype = float ),
			array( self.dayData[day][low][time-(28+1): time], dtype = float ),
			array( self.dayData[day][close][time-(28+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getWILLRpasttime(self,time):	 
		day = self.day
		real = talib.WILLR( array( self.dayData[day][high][time-(13+1): time], dtype = float ),
			array( self.dayData[day][low][time-(13+1): time], dtype = float ),
			array( self.dayData[day][close][time-(13+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

##################################################################################
# past_time -> Volume Indicator Functions
	def getADpasttime(self,time):	 
		day = self.day
		real = talib.AD( array( self.dayData[day][high][time-(0+1): time], dtype = float ),
			array( self.dayData[day][low][time-(0+1): time], dtype = float ),
			array( self.dayData[day][close][time-(0+1): time], dtype = float ),
			array( self.dayData[day][volume][time-(0+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getADOSCpasttime(self,time):	 
		day = self.day
		real = talib.ADOSC( array( self.dayData[day][high][time-(9+1): time], dtype = float ),
			array( self.dayData[day][low][time-(9+1): time], dtype = float ),
			array( self.dayData[day][close][time-(9+1): time], dtype = float ),
			array( self.dayData[day][volume][time-(9+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

##################################################################################
# past_time -> Volatility Indicator Functions
	def getATRpasttime(self,time):	 
		day = self.day
		real = talib.ATR( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)

	def getNATRpasttime(self,time):	 
		day = self.day
		real = talib.NATR( array( self.dayData[day][high][time-(14+1): time], dtype = float ),
			array( self.dayData[day][low][time-(14+1): time], dtype = float ),
			array( self.dayData[day][close][time-(14+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)	

	def getTRANGEpasttime(self,time):	 
		day = self.day
		real = talib.TRANGE( array( self.dayData[day][high][time-(1+1): time], dtype = float ),
			array( self.dayData[day][low][time-(1+1): time], dtype = float ),
			array( self.dayData[day][close][time-(1+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)	

##################################################################################
# past_time -> Cycle Indicator Functions
	def getHT_DCPERIODpasttime(self,time):	 
		day = self.day
		real = talib.HT_DCPERIOD( array( self.dayData[day][close][time-(32+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)	

	def getHT_DCPHASEpasttime(self,time):	 
		day = self.day
		real = talib.HT_DCPERIOD( array( self.dayData[day][close][time-(32+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)	

	def getHT_PHASORpasttime(self,time):	 
		day = self.day
		real = talib.HT_DCPERIOD( array( self.dayData[day][close][time-(32+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)	

	def getHT_SINEpasttime(self,time):	 
		day = self.day
		real = talib.HT_DCPERIOD( array( self.dayData[day][close][time-(32+1): time], dtype = float ) ) 
		return self.deleteNanValues(real)
