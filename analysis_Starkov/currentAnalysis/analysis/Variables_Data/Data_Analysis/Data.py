# coding=utf-8
from datetime import datetime
from numpy import isnan, invert, zeros

##################################################################################
nbCompaniesToFetch = 1561

# variables used by signals functions
date = 0
time = 1
open_ = 2
high = 3
low = 4
close = 5
volume = 6

DATE_FMT = '%Y-%m-%d'
TIME_FMT = '%H:%M:%S'
##################################################################################


class Data(object):

	def __init__(self,varObj):
		self.varObj = varObj
		self.symbol = ''
		self.companyNumber = None #which company to analys
		self.day = None
		#
		self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = (None for _ in range(7))
		self.dayData = []
		self.companiesList = []
		self.readCompaniesList()


##################################################################################
	def createAttributsAndClassesDT(self):
		longueur = self.varObj.input_number
		hauteur = 0
		for day in xrange(self.varObj.trainDay_Start,self.varObj.trainDay_End):
			hauteur += len(self.dayData[day][close])-self.varObj.startAnalysisValue-self.varObj.futur_time
		#
		attributs = zeros([hauteur, longueur], dtype = float)
		classes = zeros([1,hauteur], dtype = float)[0]
		return attributs, classes
	
##################################################################################
# read list of symbol
	# reads the names of the indexes from a file and saves it in companiesList
	def readCompaniesList(self):
		f = open(self.varObj.companiesList_file,"r")
		i = 0
		while i < nbCompaniesToFetch:
			string = f.readline()
			if "^" in string: continue
			self.companiesList.append(string.rstrip().replace("/","-"))
			i += 1
		f.close()

##################################################################################
#load data
	def loadCompanyData(self,companyNumber):
		self.companyNumber = companyNumber
		self.symbol = self.companiesList[self.companyNumber]
		self.loadData()
		self.dayData = self.separateDataPerDay()

	# reads a CSV file
	def loadData(self):
		self.date,self.time,self.open_,self.high,self.low,self.close,self.volume = ([] for _ in range(7))
		for line in open(self.varObj.companiesData_dir+self.symbol,'r'):
			ds,ts,open_,high,low,close,volume = line.rstrip().split(',')
			dt = datetime.strptime(ds+' '+ts,DATE_FMT+' '+TIME_FMT)
			self.append(dt,open_,high,low,close,volume)

	# appends the data to the object
	def append(self,dt,open_,high,low,close,volume):
		self.date.append(dt.date())
		self.time.append(dt.time())
		self.open_.append(float(open_))
		self.high.append(float(high))
		self.low.append(float(low))
		self.close.append(float(close))
		self.volume.append(int(volume))

##################################################################################
# separate data per week
	# get pourcentage of high - low changement
	# this function groupes days of one week by comparing variables day1 and day2
	# if day1 if smaller than day2, then day2 is still in the same week day as day1
	# otherwise a new week starts                                                                  
	def separateDataPerDay(self):
		dayData = []
		# adding the first date
		day1 = int(self.date[0].strftime('%u'))
		tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume = self.createWeekDataList()
		self.appendWeekDataList(tmp_date, tmp_time, tmp_open, tmp_high, tmp_low, tmp_close, tmp_volume, 0)
		#
		for i in xrange(1, len(self.date)):
			day2 = int(self.date[i].strftime('%u'))
			if day1 != day2: # check if new week starts
				dayData.append([tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume])
				#
				tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume = self.createWeekDataList()
				self.appendWeekDataList(tmp_date, tmp_time, tmp_open, tmp_high, tmp_low, tmp_close, tmp_volume, i)
			else:				
				self.appendWeekDataList(tmp_date, tmp_time, tmp_open, tmp_high, tmp_low, tmp_close, tmp_volume, i)
			day1 = day2 # change day1 to day2
		dayData.append([tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume])
		return dayData
			
	def appendWeekDataList(self,tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume,i):
		tmp_date.append(self.date[i])
		tmp_time.append(self.time[i])
		tmp_open.append(self.open_[i])
		tmp_high.append(self.high[i])
		tmp_low.append(self.low[i])
		tmp_close.append(self.close[i])
		tmp_volume.append(self.volume[i])
		
	def createWeekDataList(self):
		tmp_date = list()
		tmp_time = list()
		tmp_open = list()
		tmp_high = list()
		tmp_low = list()
		tmp_close = list()
		tmp_volume = list()
		return tmp_date,tmp_time,tmp_open,tmp_high,tmp_low,tmp_close,tmp_volume
##################################################################################
# processData
	# deletes NaN values in a vector
	def deleteNanValues(self,vect):
		return vect[invert(isnan(vect))]
