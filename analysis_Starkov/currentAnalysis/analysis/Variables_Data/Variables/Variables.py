class Variables():

    def __init__(self):
        self.past_time = 1
        self.futur_time = 10
############################################################################################################
        self.input_number = 9
        self.SBN_Input_number = 8

        # data variable
        self.startAnalysisValue = 70

        # weeks to train - test -> totWeek = 69
        self.trainDay_Start = 0
        self.trainDay_End = 33
        self.testDay = 33
        
        # companies to train - test
        self.trainTestCompany = 20
        
        # init monney
        self.initMonney = 10000
############################################################################################################
        algoTest_dir = "../"
        main_dir = "../../"
        
        Results = algoTest_dir+"Results/"
        self.classifier_tree_dir = Results+"tree/"
        
        self.min_Tree_File = self.classifier_tree_dir+"DTMin"
        self.max_Tree_File = self.classifier_tree_dir+"DTMax"
        
        self.strategy_plot_dir = Results+"Plot/"
        self.gain_dir = Results+"Gain/"
        self.realGain_dir = Results+"realGain/"
        self.realGain_pourcent_dir = Results+"realGain_pourcent/"
        self.pourcent_dir = self.realGain_pourcent_dir+"pourcent/"
        self.symbol_BuySell_dir = self.realGain_pourcent_dir+"symbol_BuySell/"
        self.treeAnalysisCurves_dir = Results+"treeAnalysisCurves/"
############################################################################################################
        self.companiesList_file = main_dir+"companiesList/companiesList.txt"
        self.companiesData_dir = main_dir+"companiesData/"
############################################################################################################
        self.corrcoeff_dir = Results+"corrcoef/"
############################################################################################################
