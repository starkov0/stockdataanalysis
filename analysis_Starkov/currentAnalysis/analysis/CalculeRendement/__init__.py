import csv
from os import listdir, getcwd
from os.path import isfile, join
from numpy import sum, mean, max, min, array
import pickle

class CalculeRendement(object):
    
    def __init__(self,varObj,dataObj):
        super(CalculeRendement, self).__init__()
        self.varObj = varObj
        self.dataObj = dataObj
        
###############################################################################
    def readCSV(self,name):
        data = []
        resultFile = open(name,'rb')
        rd = csv.reader(resultFile, dialect='excel')
        for row in rd:
            data.append(row[1])
        return data
    
    def writeCSV(self,data,fname):
        pkl_file = open(fname,'wb')
        pickle.dump(data,pkl_file)
    
    def getName(self,name):
        data = []
        resultFile = open(name,'rb')
        rd = csv.reader(resultFile, dialect='excel')
        for row in rd:
            data.append(row[0])
        return data

###############################################################################
    def getGainsAnalysis(self,data,names):
        tmp = []
        for i in xrange(len(data)):
            positif = sum([1 for x in data[i] if x > 0])
            negatif = sum([1 for x in data[i] if x < 0])
            tmp.append([names[i], positif, negatif, sum(data[i])])
        return tmp
    
    def getGood(self,data):
        good = []
        for d in data:
            if (d[1] > d[2]) and (d[3] > 0):
                good.append(d)
        return good
    
    def getIndex(self,data,names):
        ret = []
        for d in data:
            ret.append(self.dataObj.companiesList.index(d[0]))
        return ret 
    
    def getFromIndexes(self,data,indexes):
        ret = []
        for i in indexes:
            ret.append(data[i])
        return ret
###############################################################################

    def getIndexesFromGoodGain(self,semaineDebut,semaineFin):
        mypath = self.varObj.gain_dir
        onlyfiles = ["gain"+str(semaine)+".csv" for semaine in range(semaineDebut,semaineFin)]
        totalNames = self.getName(mypath+"/"+onlyfiles[1])
        #
        totalGain = []
        data = array(self.readCSV(mypath+"/"+onlyfiles[1]), dtype=float)-self.varObj.initMonney
        for d in data:
            totalGain.append([d])
        for i in xrange(1,len(onlyfiles)):
            data = array(self.readCSV(mypath+"/"+onlyfiles[i]), dtype=float)
            for i in xrange(len(data)):
                totalGain[i].append(list(data-self.varObj.initMonney)[i])
        #
        GainPre = self.getGainsAnalysis(totalGain,totalNames)
        goodGainPre = self.getGood(GainPre)
        goodGainPre = sorted(goodGainPre, key=lambda gain: gain[3])
        goodGainPre = goodGainPre[len(goodGainPre)-100:len(goodGainPre)]
        indexesGainPre = self.getIndex(goodGainPre, totalNames)
        return indexesGainPre
