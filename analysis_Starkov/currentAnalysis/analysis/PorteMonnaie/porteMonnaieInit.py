from csv import writer

class porteMonnaieInit(object):
    
    def __init__(self,varObj, dataObj, name):
        super(porteMonnaieInit, self).__init__()
        self.my_shares = dict()
        self.varObj = varObj
        self.dataObj = dataObj
        self.recv_ordre_list = []
        self.symbol = None
        self.monney = self.varObj.initMonney
        self.name = name
    
##################################################################################            
    def setStrategieCommunication(self,recv_ordre):
        self.recv_ordre = recv_ordre

##################################################################################
    def writeCSV(self,data,name):
        resultFile = open(name,'a')
        wr = writer(resultFile, dialect='excel')
        wr.writerow([self.dataObj.symbol,data,])