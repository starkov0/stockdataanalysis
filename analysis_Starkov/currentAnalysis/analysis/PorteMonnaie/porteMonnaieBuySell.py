from porteMonnaieInit import porteMonnaieInit

class porteMonnaieBuySell(porteMonnaieInit):
    
    def __init__(self,varObj, dataObj, name):
        super(porteMonnaieBuySell, self).__init__(varObj, dataObj, name)

##################################################################################            
    def calculateCost(self,nb_shares,price_per_share):
        flat_rate = 0.005*nb_shares #Flat Rate
        if flat_rate < 1:
            flat_rate = 1
        costTot = flat_rate
        
        cost_plus = 0.0035*nb_shares #Cost Plus
        costTot += cost_plus
        
        costTot += price_per_share * nb_shares #Transaction Cost
        return costTot

    def buyShares(self, nb_shares, price_per_share, symbol):
        if nb_shares*price_per_share > self.monney:
            nb_shares = int(self.monney/price_per_share)-1
        cost = self.calculateCost(nb_shares, price_per_share)
        self.monney -= cost
        if symbol not in self.my_shares:
            self.my_shares[symbol] = nb_shares
        else:
            self.my_shares[symbol] += nb_shares

    def sellShares(self, price_per_share, symbol):
        if symbol in self.my_shares:
            if self.my_shares[symbol] != 0:
                cost = self.calculateCost(self.my_shares[symbol], price_per_share)
                self.monney += cost
                self.my_shares.pop(symbol, None)
