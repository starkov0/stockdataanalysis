from porteMonnaieBuySell import porteMonnaieBuySell

class porteMonnaieUse(porteMonnaieBuySell):
    
    def __init__(self,varObj, dataObj, name):
        super(porteMonnaieUse, self).__init__(varObj, dataObj, name)
        self.nb_shares = 100
        
##################################################################################
    def getOrdre(self):
        ordre = self.recv_ordre.recv()
        return ordre
    
    def usePorteMonnaie(self):
        ordre = self.getOrdre()
        
        while ordre != "stop":
            if ordre[0] == "buy":
                self.buyShares(ordre[1], ordre[2], ordre[3])
            elif ordre[0] == "sell":
                self.sellShares(ordre[1], ordre[2])
            ordre = self.getOrdre()
        self.writeCSV(self.monney, self.name+"gain"+str(self.varObj.testDay)+".csv")
