from Variables_Data.Variables.Variables import Variables
from Variables_Data.Data_Analysis.Indicator import Indicator

class testSeuils(object):
    def __init__(self):
        super(testSeuils,self).__init__()
        self.varObj = Variables()
        self.dataObj = Indicator(self.varObj)
        
    def createTimeVector(self):
        self.dataObj.loadCompanyData(0)
        startAnalysisValue = self.varObj.startAnalysisValue
        length = len(self.dataObj.dayData[self.varObj.testDay][0])
        time = range(startAnalysisValue,length)
        return time
    
    def loadCompany(self,n):
        self.dataObj.loadCompanyData(n)
        
t = testSeuils()
print t.dataObj.loadCompanyData(0)
print t.dataObj.companiesList

