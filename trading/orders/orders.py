#Orders
#Nicolas Vachicouras
#Description: Allows to make orders on IB: Buy or Sell



class orders(object):
    def __init__(self,varObj):
        super(orders, self).__init__()
        self.varObj = varObj

    
    def buy(self):
        myEWrapper = syncEWrapper()
 
        myEClientSocket = EClientSocket(myEWrapper)
 
        connect(0,myEClientSocket)
 
        tickerVector = ['GOOG','IBM','CSCO','YHOO','MSFT','PEP','AAPL']
 
        bsVector = ['BUY','SELL']
 
        while True:
            ticker = random.sample(tickerVector,1)[0]
     
            ACTION  = random.sample(bsVector,1)[0]
     
            quantity = 1
     
            run(ticker, ACTION, quantity, 0, myEWrapper, myEClientSocket)
     
            print ACTION, ticker

    def run(ticker, order_type, quantity, clientID, myEWrapper, myEClientSocket):
        myEClientSocket.reqIds(1)
        idx = myEWrapper.nextId
        stkContract = contract(ticker)
        stkOrder = makeOrder( order_type, idx, 'DAY', 'MKT', clientID, quantity)
        myEClientSocket.placeOrder(idx, stkContract, stkOrder)
 
  
        
