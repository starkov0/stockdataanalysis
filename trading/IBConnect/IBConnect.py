import ib.ext.EClientSocket
from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import ibConnection, message
from multiprocessing import Process
from time import sleep
import time
import datetime

class IBConnect(Process):
    
    def __init__(self,varObj,traded_companies_list):
        super(IBConnect,self).__init__()
        self.varObj = varObj
        self.traded_companies_list = traded_companies_list
        self.init_Var()
        self.init_con()        
    
    def init_Var(self):
        self.nextValidId=0
        self.contract_list = None
        self.time_sleep = None
        self.communicationIB_dict = None
        self.trade_list = []
    
    def init_con(self):
        self.con = ibConnection()
        self.con.registerAll(self.watcher)
        self.con.unregister(self.watcher, message.registry['tickSize'], message.registry['tickPrice'],
                       message.registry['tickString'], message.registry['tickOptionComputation'])
        self.con.register(self.my_BidAsk, message.registry['tickPrice'])

        self.con.connect()
            
#############################################################################################       
    def set_time_sleep(self,time_sleep):
        self.time_sleep = time_sleep
        
    def setPipeCommunication(self,communicationIB_dict):
        self.communicationIB_dict = communicationIB_dict

    def setQueueCommunication(self,get_order):
        self.get_order = get_order

#############################################################################################       
    def watcher(self,msg):
        print msg

    def my_BidAsk(self,msg):
        bidAsk = None
        if msg.field == 1:
            bidAsk = "bid"
        elif msg.field == 2:
            bidAsk = "ask"
        else:
            bidAsk = "none"

        print "-------------Start-my_BidAsk--------------"
        print 'msg.TickerId:', msg.tickerId
        print 'msg.price:', msg.price
        print 'bidAsk:', bidAsk
        print "self.traded_companies_list: ", self.traded_companies_list
        print "self.traded_companies_list[msg.tickerId]:", self.traded_companies_list[msg.tickerId]
        print "self.communicationIB_dict[self.traded_companies_list[msg.tickerId]]:", self.communicationIB_dict[self.traded_companies_list[msg.tickerId]]    
        print "-------------End-my_BidAsk----"

        self.communicationIB_dict[self.traded_companies_list[msg.tickerId]].send([msg.price,bidAsk])

#############################################################################################    
    def generate_contracts(self):
        self.contract_list = []
        for symb in self.traded_companies_list:
            self.contract_list.append([symb, 'STK', 'SMART', 'USD', '', 0.0, ''])
              
    def makeStkContract(self,contractTuple):
        newContract = Contract()
        newContract.m_symbol = contractTuple[0]
        newContract.m_secType = contractTuple[1]
        newContract.m_exchange = contractTuple[2]
        newContract.m_currency = contractTuple[3]
        newContract.m_expiry = contractTuple[4]
        newContract.m_strike = contractTuple[5]
        newContract.m_right = contractTuple[6]
        return newContract
    
#############################################################################################                  
    def connectIB(self):
        print '* * * * REQUESTING MARKET DATA * * * *'
        for tickId in xrange(len(self.contract_list)):
            stkContract = self.makeStkContract(self.contract_list[tickId])
            self.con.reqMktData(tickId, stkContract, '', False)
                    
    def disconnectRestIB(self):
        print '* * * * CANCELING MARKET DATA * * * *'
        for tickId in xrange(len(self.contract_list)):
            self.con.cancelMktData(tickId)
            self.communicationIB_dict[self.traded_companies_list[tickId]].send("stop")
            
    def disconnectSymbolIB(self,symbol):
        print '---------Start-disconnectSymbolIB----------------'
        print "self.communicationIB_dict[symbol]: ", self.communicationIB_dict[symbol]
        print "self.traded_companies_list[symbol]: ", self.traded_companies_list[symbol]
        print "self.communicationIB_dict: ", self.communicationIB_dict
        print "self.traded_companies_list: ", self.traded_companies_list
        print '---------End-disconnectSymbolIB----------------'
        print '* * * * CANCELING MARKET DATA : '+symbol+' * * * *'
        tickId = self.traded_companies_list.index(symbol)
        # cancel connection
        self.con.cancelMktData(tickId)
        # remove symbol from list of companies
        if symbol in self.traded_companies_list: 
            self.traded_companies_list.remove(symbol)
        # remove symbol from dictionnary fo  of companies
        if symbol in self.communicationIB_dict.keys():
            del self.communicationIB_dict[symbol]

#############################################################################################                  
    def run(self):
        self.generate_contracts()
        self.connectIB()
        time = self.getCurrentTime()
        while len(self.traded_companies_list) > 0 and time < 3600*7:
            symbol, order_type, quantity = self.recvOrder_func()
            if order_type == "STOP":
                self.disconnectSymbolIB(symbol)
            else:
                self.sendOrder(symbol, order_type, quantity)
                if order_type == "SELL":
                    self.disconnectSymbolIB(symbol)
        self.disconnectRestIB()
    
    def recvOrder_func(self):
        order = self.get_order.get()
        return order[0], order[1], order[2]
    
    def getCurrentTime(self):
        str_date= str(datetime.date.today())
        date=str_date.split('-')
        timestamp_now=round(time.time())
        open_time=datetime.datetime(int(date[0]),int(date[1]),int(date[2]),15,30,0)
        timestamp_open=time.mktime(open_time.timetuple())
        return int(timestamp_now-timestamp_open)
    
#############################################################################################
    #BUY/SELL ORDERS

    def contract(self,sym):
        c = Contract()
        c.m_symbol = sym.upper()
        c.m_secType = 'STK'
        c.m_exchange = 'SMART'
        c.m_currency = 'USD'
        return c

    #Make a Buy or Sell Order
    def sendOrder(self,symbol,order_type,quantity):
        #self.con.reqIds(1)
        f1 = open('./data/NextValidId.txt', 'r')
        text=f1.readline()
        orderID=int(text)
        f1.close()

        f1 = open('./data/NextValidId.txt', 'w+')
        f1.write(str(orderID+1))
        f1.close()

        clientID=0
        stkContract = self.contract(symbol)
        stkOrder = self.makeOrder( order_type, orderID, 'DAY', 'MKT', clientID, quantity)
        self.con.placeOrder(orderID, stkContract, stkOrder)
        print order_type, symbol, "(", orderID, ")"

    def makeOrder(self,action, orderID, tif, orderType, clientID, quantity):
        newOrder = Order()
        newOrder.m_orderId = orderID
        newOrder.m_clientId = clientID
        newOrder.m_permid = 0
        newOrder.m_action = action
        newOrder.m_lmtPrice = 0
        newOrder.m_auxPrice = 0
        newOrder.m_tif = tif
        newOrder.m_transmit = True
        newOrder.m_orderType = orderType
        newOrder.m_totalQuantity = quantity
        return newOrder


