# coding=utf-8
import urllib
import sys
import pickle as pppickle
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType

##################################################################################
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

##################################################################################

class Google_Quotes_20(object):

    def __init__(self,varObj):
        super(Google_Quotes_20,self).__init__()
        self.date,self.time,self.high,self.low,self.close = ([] for _ in range(5))
        self.listSymbols = []
        self.company_limits= {}
        self.interval_seconds=1200
        self.num_days=1
        self.progress=1
        self.varObj = varObj

    # reads the names of the companies from a file and saves it in listSymbols
    def getCompanies(self,liste):
        self.listSymbols=liste

    # fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
    def fetchCompanyData20(self):
        sys.stdout.write("[%d]" % (len(self.listSymbols)))
        #sys.stdout.flush()

        pool1 = Pool(20)
        async_results1 = [ pool1.apply_async(self.fetchCompanyData20Threading, (i,)) for i in self.listSymbols ]
        map(ApplyResult.wait, async_results1)

        self.write_Pickle(self.company_limits, self.varObj.list_20_file)
        
    def fetchCompanyData20Threading(self,symbol):
        #sys.stdout.write(".")
        #sys.stdout.flush()
        self.date,self.time,self.high,self.low = ([] for _ in range(4))
        high,low = self.fetchData20(symbol)

        self.company_limits[symbol,'high20']=high
        self.company_limits[symbol,'low20']=low
            
    def write_Pickle(self,data,name):
        pppickle.dump(data, open(name,'wb'))
        
    # fetches data for one company at the time
    def fetchData20(self,symbol):
        url_string = "http://www.google.com/finance/getprices?q={0}".format(symbol)
        url_string += "&i={0}&p={1}d&f=d,h,l".format(self.interval_seconds,self.num_days)
        csv = urllib.urlopen(url_string).readlines()

        if(len(csv)>=8):
            _,high,low = csv[7].split(',')
            return float(high), float(low)
    
    #Returns the Limits
    def get_limits(self):
        return self.company_limits


