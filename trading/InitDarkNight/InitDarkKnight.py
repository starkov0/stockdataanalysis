#Init Dark Knight v.1.0
#Nicolas Vachicouras
#Description: Get List and 4 limits for tradable companies

from Google_Quotes_Pre import Google_Quotes_Pre
from Google_Quotes_20 import Google_Quotes_20
import time
import pickle
import os

class InitDarkKnight(object):
    def __init__(self,varObj):
        super(InitDarkKnight, self).__init__()
        self.varObj = varObj
        self.init_lists()
        self.init_lists()
        
    def init_lists(self):
        self.symboles=[]
        self.company_limits_PRE_20={}
        self.company_limits_PRE={}
        self.company_limits_20={}
        self.volatility={}
        self.firms_symb={}
        self.firms_trade={}

#############################################################################################
    def read_Symbol_List_companies(self):
        f = open(self.varObj.companyList_file, 'r')
        symboles = []
        for line in f:
            symboles.append(line.split('\n')[0])
        return list(set(symboles))
            
    def read_Symbol_Dict_companies(self,dictio):
        symboles = []
        for key in dictio.keys():
            symboles.append(key[0])
        return list(set(symboles))
    
    def get_Intersection_Lists(self,a,b):
        return list(set(a) & set(b))

#############################################################################################            
    def fetch_PRE_data(self):
        start_time = time.time()
        print '\nGenerating PRE Data'
        #
        if not os.path.exists(self.varObj.list_previous_day_file):
            #Get High Low Close Volatility from Google for PRE -> file
            getter = Google_Quotes_Pre(self.varObj)
            getter.getCompanies(self.symboles)
            getter.fetchCompanyDataPre()

        end_time = time.time()
        print("Execution: %5.2f seconds" % (end_time - start_time))

    def fetch_20_data(self):
        start_time = time.time()
        print '\nGenerating 20 Minutes Data'
        if not os.path.exists(self.varObj.list_20_file):
            #Get High Low from Google for the first 20 minutes -> self.var
            getter = Google_Quotes_20(self.varObj)
            getter.getCompanies(self.symboles)
            getter.fetchCompanyData20()
            self.company_limits_20=getter.get_limits()
        else:
            self.company_limits_20=pickle.load(open(self.varObj.list_20_file,'rb'))

        end_time = time.time()
        print("Execution: %4.2f seconds" % (end_time - start_time))

#############################################################################################
    def get_PRE_limit_AND_Volatility(self):
        f = open(self.varObj.list_previous_day_file, 'r')
        for line in f:
            data_split=line.split(',')
            self.get_Volatility(data_split)
            self.get_PRE_limit(data_split)
        f.close()
        
    def get_PRE_limit(self,data_split):
        symb=data_split[0]
        previous_low = float(data_split[2])
        previous_high = float(data_split[1])
        previous_close = float(data_split[3])
        daily_pivot_price= (previous_high+previous_low+previous_close)/3.
        second_number=(previous_high+previous_low)/2.
        daily_pivot_differential=abs(second_number-daily_pivot_price)
        #
        self.company_limits_PRE[symb,'highPre']=daily_pivot_price+daily_pivot_differential
        self.company_limits_PRE[symb,'lowPre']=daily_pivot_price-daily_pivot_differential
        
    def get_Volatility(self,data_split):
        symb=data_split[0]
        vol=data_split[4]
        vol=vol.split('\\')
        vol=vol[0]
        self.volatility[symb]=float(vol)
        
    def get_20_limit(self):
        #self.company_limits_PRE_20 = self.company_limits_PRE
        #Calculate the high and low
        for symb in self.symboles:
            self.company_limits_PRE_20[symb,'high20']=self.company_limits_20[symb,'high20']
            self.company_limits_PRE_20[symb,'low20']=self.company_limits_20[symb,'low20']
            self.company_limits_PRE_20[symb,'highPre']=self.company_limits_PRE[symb,'highPre']
            self.company_limits_PRE_20[symb,'lowPre']=self.company_limits_PRE[symb,'lowPre']
    
    def check_limits(self,company_limits):
        #check length
        if len(company_limits)==len(self.symboles)*4:
            print 'Success'
        else:
            print("Failure (%d/%d)" % (len(company_limits)/4,len(self.symboles)))

#############################################################################################
    #Find Firms which respect the limits
    def find_good_firms(self,company_limits):
        for symb in self.symboles:
            if self.volatility[symb]>=0.2:
                if company_limits[symb,'high20'] > company_limits[symb,'lowPre'] and \
                company_limits[symb,'high20'] < company_limits[symb,'highPre']:
                    #Buy at least for winning 3 USD
                    Price=company_limits[symb,'high20']
                    qty=int(round((10000.)/Price))
                    if qty < 1:
                        qty=1
                    if qty>100:
                        qty=100

                    if 100*(company_limits[symb,'highPre'] - company_limits[symb,'high20']) >= 3 and \
                    company_limits[symb,'low20'] < company_limits[symb,'lowPre']:
                        self.firms_symb[symb]=1 #Hausse

        for symb, val in self.firms_symb.items():
            if (val==1):
                self.firms_trade[symb,'low20']=company_limits[symb,'low20']
                self.firms_trade[symb,'high20']=company_limits[symb,'high20']
                self.firms_trade[symb,'lowPre']=company_limits[symb,'lowPre']
                self.firms_trade[symb,'highPre']=company_limits[symb,'highPre']


        print ("\n%d companies will be traded today\n" % (len(self.firms_trade)/5))

        for symb,val in self.firms_symb.items():
            print symb,': ', self.firms_trade[symb,'high20'], self.firms_trade[symb,'highPre']


#############################################################################################      

    def launch_Pre(self):
        self.symboles = self.read_Symbol_List_companies()
        #
        self.fetch_PRE_data()
        self.get_PRE_limit_AND_Volatility()
        #
        self.symboles = self.read_Symbol_Dict_companies(self.company_limits_PRE)
        print str(len(self.symboles)),' Companies in Pre Data'

    def launch_20(self): # !!! Wait for 15:50 !!!

        self.fetch_20_data() #-> GOOGLE_QUOTES

        symboles = self.read_Symbol_Dict_companies(self.company_limits_20)
        self.symboles = self.get_Intersection_Lists(self.symboles, symboles)
        print str(len(self.symboles)),' Companies in 20 Data'

        #Makes all limits in matrix
        self.get_20_limit()
        #Checks that we have all data
        #self.check_limits(self.company_limits_PRE_20)

        #Find Good Firms
        self.find_good_firms(self.company_limits_PRE_20)
        self.symboles = self.read_Symbol_Dict_companies(self.firms_trade)

    def return_firms_trade(self):
        return self.firms_trade
    
    def return_symboles_list(self):
        return self.symboles

