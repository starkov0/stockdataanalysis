# coding=utf-8
import urllib
from numpy import array
import sys
from multiprocessing.dummy import Pool
from multiprocessing.pool import ApplyResult
from copy_reg import pickle
from types import MethodType

##################################################################################
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

##################################################################################

DATE_FMT = '%Y-%m-%d'
TIME_FMT = '%H:%M:%S'

##################################################################################

class Google_Quotes_Pre(object):

    def __init__(self,varObj):
        super(Google_Quotes_Pre,self).__init__()
        self.date,self.time,self.high,self.low,self.close = ([] for _ in range(5))
        self.listSymbols = []
        self.length_csv=0
        self.company_limits= {}
        self.interval_seconds=60
        self.num_days=1
        self.progress=1
        self.varObj = varObj
        
    # reads the names of the companies from a file and saves it in listSymbols
    def getCompanies(self,liste):
        self.listSymbols=liste
        
##################################################################################
    # fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
    def fetchCompanyDataPre(self):
        sys.stdout.write("[%s]" % (" " * len(self.listSymbols)))
        sys.stdout.flush()
        sys.stdout.write("\b" * (len(self.listSymbols)+1))
        #
        pool1 = Pool(20)
        async_results1 = [ pool1.apply_async(self.fetchCompanyDataPreThreading, (i,)) for i in self.listSymbols ]
        map(ApplyResult.wait, async_results1)
        data = [r.get() for r in async_results1]
        #
        sys.stdout.write("\n")
        self.write_csv(data,self.varObj.list_previous_day_file)

    # fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
    def fetchCompanyDataPreThreading(self,symbol):
        sys.stdout.write("-")
        sys.stdout.flush()
        #
        highday,lowday,closeday,vol = self.fetchDataPre(symbol)
        return [symbol,highday,lowday,closeday,vol]

    # fetches data for one company at the time
    def fetchDataPre(self,symbol):
        close_list, high_list, low_list = ([] for _ in range(3))
        url_string = "http://www.google.com/finance/getprices?q={0}".format(symbol)
        url_string += "&i={0}&p={1}d&f=d,c,h,l".format(self.interval_seconds,self.num_days)
        csv = urllib.urlopen(url_string).readlines()
        #
        for bar in xrange(7,len(csv)):
            if csv[bar].count(',')!=3: continue
            _,close,high,low = csv[bar].split(',')
            close_list.append(float(close))
            high_list.append(float(high))
            low_list.append(float(low))
        # 
        if len(high_list) >= 100:
            highday=max(high_list)
            lowday=min(low_list)
            closeday=close_list[-1]
            vol=array(close_list).std()
            return highday,lowday,closeday,vol
        else:
            return [],[],[],[]
            
##################################################################################

#     # writes a CSV
    def write_csv(self,data,name):
        with open(name,'w') as f:
            for sub_data in data:
                if sub_data[1] != []:
                    f.write(self.to_csv(sub_data))
 
    # prepares lines that have to be written in a CSV file
    def to_csv(self,sub_data):
        return ''.join(["{0},{1:.2f},{2:.2f},{3:.2f},{4:.2f}\n".format(
                    sub_data[0],sub_data[1],sub_data[2],sub_data[3],sub_data[4])])