from multiprocessing import Process
import time
import datetime
import os

class DarkNight(Process):
    
    def __init__(self,argument_list,varObj):
        super(DarkNight,self).__init__()
        self.varObj = varObj
        self.symbol = argument_list[0]
        self.lowPre = argument_list[1]
        self.highPre = argument_list[2]
        self.low20 = argument_list[3]
        self.high20 = argument_list[4]
        self.initVar()
        
        self.tmp = 0
        
    def initVar(self):
        self.inTrade = False
        self.recv_message = None
        self.buyPrice = None
        self.qty = None
        self.buyTime = None
        self.inc=0
        self.incTime=0
        self.continueTrading = True
        
############################################################################################
    def setPipeCommunication(self,recv_price):
        self.recv_price = recv_price
        
    def setQueueCommunication(self,send_order):
        self.send_order = send_order
    
############################################################################################
    def run(self):
        self.getCurrentPrice()

############################################################################################
    def getCurrentTime(self):
        str_date= str(datetime.date.today())
        date=str_date.split('-')
        timestamp_now=round(time.time())
        open_time=datetime.datetime(int(date[0]),int(date[1]),int(date[2]),15,30,0)
        timestamp_open=time.mktime(open_time.timetuple())
        return int(timestamp_now-timestamp_open)

    def getCurrentPrice(self):
        while self.continueTrading:
            self.recv_message = self.recv_price.recv()
            #Added by Nicolas, to take away at the end:
            time = int(self.getCurrentTime())
            print  time,': ', self.recv_message[0], self.recv_message[1], "(",self.symbol,")"
            self.performAction(self.recv_message[0],self.recv_message[1])
                
############################################################################################
    def sendOrder_func(self,action,qty):
        self.send_order.put([self.symbol,action,qty])
    
    def sendStopProcess_func(self):
        self.send_order.put([self.symbol,"STOP",0])
    
############################################################################################
    def writeText(self,txt):
        # printing text
        print txt
        # writing text
        f = open(self.varObj.Transaction_dir+self.symbol+'.txt', 'w+')
        f.write(txt)
        f.close()

############################################################################################
    def performAction(self,price,bidAsk):
        
        #Buy Strategy
        time = int(self.getCurrentTime())

        #print time,' (',self.symbol,'-',bidAsk,'): ', price, ', ', self.high20,', ', self.highPre

        # if time went too far, stop that shit!
        if time>3600 and not self.inTrade:
            self.continueTrading = False
            self.sendStopProcess_func()
        #As long as we are in the first 60 minutes, that we have enough money and we haven't traded for that company yet
        if time<=3600 and not self.inTrade and bidAsk=='ask':

            if time>self.incTime:
                if(price>self.high20):
                    if self.incTime==0:
                        self.inc=self.inc+1
                    else:
                        self.inc=self.inc+(time-self.incTime)
                else:
                    self.inc=0
                self.incTime=time

            #If we are between the high20 limit and the highPre limit --> we buy
            if(price>self.high20 and price<self.highPre):# and self.inc > 30):
                self.inTrade = True
                self.buyPrice = price
                self.buyTime = time
                #Calculate how many stocks we buy
                self.qty=int(round((10000.)/self.buyPrice))
                if self.qty < 1:
                    self.qty=1
                if self.qty>100:
                    self.qty=100
                # text
                txt=self.symbol+', '+str(self.qty)+', '+str(self.buyTime)+', '+str(self.buyPrice)  
                self.writeText(txt)
                # SEND ORDER
                self.sendOrder_func("BUY", self.qty)

        # Sell Strategy
        if self.inTrade and bidAsk=='bid':
            if(price<=float(self.buyPrice)*(1-0.03)) or (price>self.highPre) or (time>int(self.buyTime)+120*60):
                # text
                txt=self.symbol+', '+str(self.qty)+', '+str(self.buyTime)+', '+str(self.buyPrice) +', '+str(time)+', '+str(price)
                self.writeText(txt)                  
                # SEND ORDER
                self.sendOrder_func("SELL", self.qty)
                # STOP PROCESS
                self.continueTrading = False

                    
