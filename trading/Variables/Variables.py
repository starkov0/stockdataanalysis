class Variables(object):
    
    def __init__(self):
        super(Variables,self).__init__()
        
        ##########################################################
        self.data_dir = "./data/"
        self.companyList_dir = self.data_dir + "companyList/"
        self.historicalData_dir = self.data_dir + "historicalData/"
        self.PRE_20_data_dir = self.data_dir + "PRE_20_data/"
        
        ##########################################################
        self.companyList_file = self.companyList_dir + "companyList.txt"
        self.list_previous_day_file = self.PRE_20_data_dir + "list_previous_day.txt"
        self.list_20_file = self.PRE_20_data_dir + "list_20.txt"
        
        ##########################################################
        self.Transaction_dir = 'Transactions/'
        