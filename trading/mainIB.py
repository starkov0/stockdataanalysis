from multiprocessing.queues import Pipe, SimpleQueue
from algorithme.DarkNight import DarkNight
from Variables.Variables import Variables
from analysis.analysisTrades import analysisTrades
from InitDarkNight.InitDarkKnight import InitDarkKnight
from IBConnect.IBConnect import IBConnect
import os
import datetime
import time
from time import sleep

class mainIB(object):
    
    def __init__(self):
        super(mainIB,self).__init__()
        self.init_Data()
        self.init_Objects()
        
    def init_Data(self):
        self.traded_companies_list = None
        self.traded_companies_limits_dict = None
        self.queue = None
        
    def init_Objects(self):
        self.init_Algorithm = None
        self.list_Algorithm = []
        self.iBConnect = None
        self.varObj = Variables()

############################################################################################
    def init_DarkNight(self):
        self.init_Algorithm = InitDarkKnight(self.varObj)

        self.init_Algorithm.launch_Pre()

        #Wait for the right time
        t=-self.getCurrentTime()
        print t
        
        #sleep(t+60*20)
        
        #sleep(20)

        self.init_Algorithm.launch_20()
    
    def get_traded_companies_list_dict(self):
        self.traded_companies_list = self.init_Algorithm.return_symboles_list()
        self.traded_companies_limits_dict = self.init_Algorithm.return_firms_trade()

############################################################################################
    def createListDarkNight(self):
        pre_20 = ['lowPre','highPre','low20','high20']
        for symb in self.traded_companies_list:
            argument_Algorithm_list = [symb]
            for limit in pre_20:
                argument_Algorithm_list.append(self.traded_companies_limits_dict[symb,limit])
            self.list_Algorithm.append(DarkNight(argument_Algorithm_list, self.varObj))
           
    def createIBConnect(self):
        self.iBConnect = IBConnect(self.varObj,self.traded_companies_list)
        self.iBConnect.set_time_sleep(3600*3)
    
############################################################################################
    def setCommunication(self):
        # sending price communication -> PIPE
        communicationIB_dict = dict()
        for dark in self.list_Algorithm:
            recv_price, send_price = Pipe()
            communicationIB_dict[dark.symbol] = send_price
            dark.setPipeCommunication(recv_price)
        self.iBConnect.setPipeCommunication(communicationIB_dict)
        # sending orders communcation -> QUEUE
        queue = SimpleQueue()
        for dark in self.list_Algorithm:
            dark.setQueueCommunication(queue)
        self.iBConnect.setQueueCommunication(queue)
        

############################################################################################
    def startIBConnect(self):
        self.iBConnect.start()
    
    def startListDarkNight(self):
        for dark in self.list_Algorithm:
            dark.start()
            
    def joinIBConnect(self):
        self.iBConnect.join()
 
############################################################################################  

    def analysisTrades(self):
        self.analyse = analysisTrades(self.varObj)
        self.analyse.cleanTrades()

    def makeOrder(self):
        self.iBConnect.sendOrder()

    def getCurrentTime(self):
        str_date= str(datetime.date.today())
        date=str_date.split('-')
        timestamp_now=round(time.time())
        open_time=datetime.datetime(int(date[0]),int(date[1]),int(date[2]),15,30,0)
        timestamp_open=time.mktime(open_time.timetuple())
        return int(timestamp_now-timestamp_open)

    def getCurrentTimePre(self):
        str_date= str(datetime.date.today())
        date=str_date.split('-')
        timestamp_now=round(time.time())
        open_time=datetime.datetime(int(date[0]),int(date[1]),int(date[2]),13,10,0)
        timestamp_open=time.mktime(open_time.timetuple())
        return int(timestamp_now-timestamp_open)


m = mainIB()

m.init_DarkNight()
m.get_traded_companies_list_dict()
m.createListDarkNight()
m.createIBConnect()
m.setCommunication()
m.startIBConnect()
m.startListDarkNight()
m.joinIBConnect()
m.analysisTrades()


