cd ..

pyreverse -o png -p 'ALL' -S mainIB ./algorithme/ ./analysis/ ./IBConnect/ ./InitDarkNight/ ./Variables/
mv classes_ALL.png ./UML/classes_ALL.png
mv classes_NAME_ONLY.png ./UML/classes_NAME_ONLY.png

pyreverse -o png -p 'NAME_ONLY' -k -S mainIB ./algorithme/ ./analysis/ ./IBConnect/ ./InitDarkNight/ ./Variables/
mv packages_ALL.png ./UML/packages_ALL.png
mv packages_NAME_ONLY.png ./UML/packages_NAME_ONLY.png
