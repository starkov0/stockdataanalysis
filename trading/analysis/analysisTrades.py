#Analysis Trades
#Nicolas Vachicouras
#Description: Analyse Trades performed during the day

import glob
import os

class analysisTrades(object):
    def __init__(self,varObj):
        super(analysisTrades, self).__init__()
        self.varObj = varObj

    
    #Goes in the folder "Transactions" and puts all the trading data in one file
    def cleanTrades(self):
        f = open('trades.txt', 'a')
        en_tete='SYMB, Time Buy, Price Buy, Time Sell, Price Sell, Qty\n'
        f.write(en_tete)
        files = glob.glob("./Transactions/*")
        for file in files:
            f1=open(file,'r')
            txt=f1.readline()
            if(txt[-1]=='\n'):
                f.write(txt)
            else:
                f.write(txt+'\n')
            f1.close()
            os.remove(file)
        f.close()