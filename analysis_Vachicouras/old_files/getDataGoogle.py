# coding=utf-8
import urllib,time,datetime
#import matplotlib.pyplot as plt
import math
import time
from numpy import array
import sys

##################################################################################

DATE_FMT = '%Y-%m-%d'
TIME_FMT = '%H:%M:%S'

##################################################################################

class Google_Quotes():

	def __init__(self):
		self.symbol = ''
		self.date,self.time,self.high,self.low,self.close = ([] for _ in range(5))
		self.listSymbols = []
		self.length_csv=0
		self.company_limits= {}
		self.interval_seconds=0
		self.num_days=0
		self.progress=1

	def setParameters(self,interval,period):
		self.interval_seconds=interval
		self.num_days=period

	# reads the names of the companies from a file and saves it in listSymbols
	def getCompanies(self,liste):
		self.listSymbols=liste

	# fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
	def fetchCompanyData20(self):
		for symbol in self.listSymbols:
			self.symbol = symbol
			sys.stdout.write('\r')
			sys.stdout.write("Progress [%5s]: %d/1440" % (self.symbol,int(self.progress)))
			sys.stdout.flush()
			self.date,self.time,self.high,self.low = ([] for _ in range(4))
			self.fetchData20()
			self.company_limits[self.symbol,'high20']=self.high
			self.company_limits[self.symbol,'low20']=self.low
			self.progress=self.progress+1
			#if self.length_csv>=8:
			#	self.write_csv()

	# fetches data for one company at the time
	def fetchData20(self):
		url_string = "http://www.google.com/finance/getprices?q={0}".format(self.symbol)
		url_string += "&i={0}&p={1}d&f=d,h,l".format(self.interval_seconds,self.num_days)
		csv = urllib.urlopen(url_string).readlines()
		#print self.symbol
		self.length_csv= len(csv)
		if(len(csv)>=8):
			offset,high,low = csv[7].split(',')
			self.high,self.low = [float(x) for x in [high,low]]

	# fetch all the companies from GOOGLE FINANCE and save it in a *.csv file
	def fetchCompanyDataPre(self):
		for symbol in self.listSymbols:
			sys.stdout.write('\r')
			sys.stdout.write("Progress [%5s]: %d/1440" % (symbol, int(self.progress)))
			sys.stdout.flush()
			self.symbol = symbol
			self.date,self.time,self.high,self.low,self.close = ([] for _ in range(5))
			self.fetchDataPre()
			if self.length_csv>=100:
				self.write_csv()
				self.progress=self.progress+1

	# fetches data for one company at the time
	def fetchDataPre(self):
		url_string = "http://www.google.com/finance/getprices?q={0}".format(self.symbol)
		url_string += "&i={0}&p={1}d&f=d,h,l,c".format(self.interval_seconds,self.num_days)
		csv = urllib.urlopen(url_string).readlines()
		old_offset=390
		#print self.symbol
		self.length_csv= len(csv)
		for bar in xrange(7,len(csv)):
			if csv[bar].count(',')!=3: continue
			offset,close,high,low = csv[bar].split(',')
			if offset[0]=='a':
				day = float(offset[1:])
				offset = 0
			else:
				offset = float(offset)
			high,low,close = [float(x) for x in [high,low,close]]
			dt = datetime.datetime.fromtimestamp(day+(self.interval_seconds*offset))
			self.append(dt,high,low,close)
			
			old_offset=offset
			old_day=day
	
	def append(self,dt,high,low,close):
		self.date.append(dt.date())
		self.time.append(dt.time())
		self.high.append(float(high))
		self.low.append(float(low))
		self.close.append(float(close))

	#Returns the Limits
	def get_limits(self):
		return self.company_limits

	# writes a CSV
	def write_csv(self):
		with open("list_previous_day.txt",'a') as f:
			f.write(self.to_csv())

	# prepares lines that have to be written in a CSV file
	def to_csv(self):
		#Calculate...
		highday=max(self.high)
		lowday=min(self.low)
		closeday=self.close[-1]
		arrayClose=array(self.close)
		vol=arrayClose.std()
		return ''.join(["{0},{1:.2f},{2:.2f},{3:.2f},{4:.2f}\n".format(
					self.symbol,
					highday,
					lowday,
					closeday,
					vol)
					])

