#Dark Knight v.1.0
#Nicolas Vachicouras
#Description: Algorithm for Trading

from random import randint

init=1000000.
capital=init

#Module 1: Get Company List
company_symb=["AAPL", "GOOG", "IBM"]

#Module 2: Calculate Previous Day limits
company_limits= {} #contains the 4 limits for each stock

#Get Data from Previous Day
for symb in company_symb:
	print(symb)

	previous_high = 80
	previous_low = 10
	previous_close = 3.3

	daily_pivot_price= (previous_high+previous_low+previous_close)/3.
	second_number=(previous_high+previous_low)/2.
	daily_pivot_differential=abs(second_number-daily_pivot_price)
	company_limits[symb,'highPre']=daily_pivot_price+daily_pivot_differential
	company_limits[symb,'lowPre']=daily_pivot_price-daily_pivot_differential


#Module 3: Get Data from first 20 minutes and calculate high/low
#Check that it is opening time between 0 and 20 minutes

#Get data for 20 minutes
company_data = {}

for id in range(1,21):
	for symb in company_symb:
		num = randint(1,46)
		if id==1:
			company_data[symb,'value']=[num]
			company_data[symb,'time']=[id]
		else:
			company_data[symb,'value'].append(num)
			company_data[symb,'time'].append(id)

#Calculate the high and low
for symb in company_symb:
	company_limits[symb,'high20']=max(company_data[symb,'value'])
	company_limits[symb,'low20']=min(company_data[symb,'value'])

#Save in File all limits

#Module 4: Find Firms which respect the limits
firms_symb={}

for symb in company_symb:
	if company_limits[symb,'high20'] > company_limits[symb,'lowPre'] and company_limits[symb,'high20'] < company_limits[symb,'highPre']:
		if (company_limits[symb,'highPre'] - company_limits[symb,'high20']) > 0.1 and company_limits[symb,'low20'] < company_limits[symb,'lowPre']:
			firms_symb[symb]=1 #HIGH
	elif company_limits[symb,'low20'] > company_limits[symb,'highPre'] and company_limits[symb,'low20'] < company_limits[symb,'lowPre']:
		if (company_limits[symb,'low20'] - company_limits[symb,'lowPre']) > 0.1 and company_limits[symb,'high20'] < company_limits[symb,'highPre']:
			firms_symb[symb]=2 #LOW

firms_symb_high=[] #Firms which respect the limits trading high
firms_symb_low=[] #Firms which respect the limits trading low

for symb, val in firms_symb.items():
	if (val==1):
		firms_symb_high.append(symb)
	else:
		firms_symb_low.append(symb)

#Module #5: Start Trading on Selected Firms
trade_high={} #Firms traded on high
trade_low={} #Firms traded on low

for symb in firms_symb_high:
	trade_high[symb,'buy']=0 		#Buy Price
	trade_high[symb,'sell']=0 		#Sell Price
	trade_high[symb,'buymin']=0 	#Buy Minute
	trade_high[symb,'sellmin']=0 	#Sell Minute
	trade_high[symb,'qty']=0 		#Quantity of stock bought

for symb in firms_symb_low:
	trade_low[symb,'buy']=0
	trade_low[symb,'sell']=0
	trade_low[symb,'buymin']=0
	trade_low[symb,'sellmin']=0
	trade_low[symb,'qty']=0


#Get Market Data for all day (390 minutes)
for t in range(21,391):
	
	# BUY STRATEGY

	#As long as we are in the first 60 minutes and that we have enough money
	if(t<=60 and capital>init/20.):
		#Strategy for High
		for symb in firms_symb_high:
			data = randint(1,46) #For the moment we get random data for price
			company_data[symb,'value'].append(data)
			company_data[symb,'time'].append(t)

			#If we haven't traded for that company yet
			if(trade_high[symb,'buy']==0):
				#If we are between the high20 limit and the highPre limit --> we buy
				if(data>company_limits[symb,'high20']) and (data<company_limits[symb,'highPre']):
					print 'BUY HIGH'
					trade_high[symb,'buy']=data
					trade_high[symb,'buymin']=t
					#Calculate how many stocks we buy
					qty=round((init/20.)/data)
					if qty < 1:
						qty=1
					trade_high[symb,'qty']=qty
					capital=capital-qty*trade_high[symb,'buy']-2

		#Strategy for Low: Similar to high, but inverted
		for symb in firms_symb_low:
			data = randint(1,46)#For the moment we get random data for price
			company_data[symb,'value'].append(data)
			company_data[symb,'time'].append(t)

			if(trade_low[symb,'buy']==0):
				if(data<company_limits[symb,'low20']) and (data>company_limits[symb,'lowPre']):
					print 'BUY LOW'
					trade_low[symb,'buy']=data
					trade_low[symb,'buymin']=t
					qty=round((init/20.)/data)
					if qty < 1:
						qty=1
					trade_low[symb,'qty']=qty
					capital=capital-qty*trade_low[symb,'buy']-2

	# SELL STRATEGY

	#Strategy for High
	for symb in firms_symb_high:
		#If we have bought but haven't sold yet
		if(trade_high[symb,'buy']>0 and trade_high[symb,'sell']==0):
			data = randint(1,46) #For the moment we get random data for price
			#Sell if we reach -3% or if we reach the highPre limit or
			#if we waited more than 120 minutes
			if(data<=trade_high[symb,'buy']*(1-0.03)) or \
			(data>=company_limits[symb,'highPre']) or \
			(t>trade_high[symb,'buymin']+120):
				print 'SELL HIGH'
				trade_high[symb,'sell']=data
				trade_high[symb,'sellmin']=t
				amount=trade_high[symb,'qty']*trade_high[symb,'buy']
				buy=trade_high[symb,'buy']
				sell=trade_high[symb,'sell']
				capital=capital+amount*(1+(sell-buy)/buy)

	#Strategy for Low: Similar to high but inverted
	for symb in firms_symb_low:
		if(trade_low[symb,'buy']>0 and trade_low[symb,'sell']==0):
			data = randint(1,46) #For the moment we get random data for price
			#Sell if we reach -3% or if we reach the lowPre limit or
			#if we waited more than 120 minutes
			if(data>=trade_low[symb,'buy']*(1+0.03)) or \
			(data<=company_limits[symb,'lowPre']) or \
			(t>trade_low[symb,'buymin']+120):
				print 'SELL LOW'
				trade_low[symb,'sell']=data
				trade_low[symb,'sellmin']=t
				amount=trade_low[symb,'qty']*trade_low[symb,'buy']
				buy=trade_low[symb,'sell'] #Inverse when short position
				sell=trade_low[symb,'buy'] #Inverse when short position
				capital=capital+amount*(1+(sell-buy)/buy)
			
