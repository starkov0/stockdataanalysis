# python unlink files that you dont need
import glob

def create_list_companys(days):  
	f = open('companyList.txt', 'w')
	files = glob.glob("historicalData_70days/*")
	for file in files:
	    num_lines = sum(1 for line in open(file))
	    if num_lines == (391*days):
	    	new_file=file.split('/')
	    	new_file=new_file[1].split('.')
	    	new_file=new_file[0] + '\n'
	    	print(new_file)
	    	f.write(new_file)

	f.close()

def read_list_companys():
	f = open('companyList.txt', 'r')
	i=0
	symb=["TEST"]
	for line in f:
		if(i==0):
			text=line.split('\n')
			symb[i]=text[0]
		else:
			text=line.split('\n')
			symb.append(text[0])
		i=i+1;
	return symb

def generate_contracts(company_symb):
	index=1
	contractDict = {}
	for symb in company_symb:
		contractDict[index] = (symb, 'STK', 'SMART', 'USD', '', 0.0, '')
		index=index+1
	return contractDict


def generate_daily_data(company_symb):
	
	f = open('list_previous_day.txt', 'w')
	#SYMB PREV_HIGH PREV_LOW PREV_CLOSE

	for symb in company_symb:
		#Read Data from file	
		name='historicalData/'+symb + '_18_11_2013.txt'
		f1=open(name,'r')
		data=[]

		for line in f1:
			data_split=line.split(',')
			price=data_split[2]
			data.append(price)
		
		#Write Data    	
		f.write(symb+', '+str(min(data))+', '+str(max(data))+', '+str(data[-1])+'\n')
    	f1.close();

	f.close();


def get_high_low_pre_limit():
	company_limits= {}
	f = open('list_previous_day.txt', 'r')

	for line in f:
		data_split=line.split(',')
		close_price=data_split[3]
		close_price=close_price.split('\\')
		close_price=close_price[0]

		symb=data_split[0]
		previous_low = float(data_split[1])
		previous_high = float(data_split[2])
		previous_close = float(close_price)

		daily_pivot_price= (previous_high+previous_low+previous_close)/3.
		second_number=(previous_high+previous_low)/2.
		daily_pivot_differential=abs(second_number-daily_pivot_price)

		company_limits[symb,'highPre']=daily_pivot_price+daily_pivot_differential
		company_limits[symb,'lowPre']=daily_pivot_price-daily_pivot_differential

	f.close()
	return company_limits
