#Init Dark Knight v.1.0
#Nicolas Vachicouras
#Description: Get List and 4 limits for tradable companies

from random import randint
from getDataGoogle import Google_Quotes
import glob
import time

class InitDarkKnight(object):
	def __init__(self):
		super(InitDarkKnight, self).__init__()
		self.symboles=[]
		self.company_limits={}
		self.volatility={}
		self.firms_symb={}
		self.firms_trade={}
		self.nb_companies=1440

	def read_list_companies(self):
		f = open('companyList.txt', 'r')
		i=0
		self.symboles=["TEST"]
		for line in f:
			if(i==0):
				text=line.split('\n')
				self.symboles[i]=text[0]
			else:
				text=line.split('\n')
				self.symboles.append(text[0])
			i=i+1

	def get_HighLowPre_limit(self):
		f = open('list_previous_day.txt', 'r')
		for line in f:
			data_split=line.split(',')
			vol=data_split[4]
			vol=vol.split('\\')
			vol=vol[0]
			symb=data_split[0]

			self.volatility[symb]=float(vol)
			previous_low = float(data_split[1])
			previous_high = float(data_split[2])
			previous_close = float(data_split[3])

			daily_pivot_price= (previous_high+previous_low+previous_close)/3.
			second_number=(previous_high+previous_low)/2.
			daily_pivot_differential=abs(second_number-daily_pivot_price)

			self.company_limits[symb,'highPre']=daily_pivot_price+daily_pivot_differential
			self.company_limits[symb,'lowPre']=daily_pivot_price-daily_pivot_differential
		f.close()

	def generate_daily_data(self):
		start_time = time.time()
		print 'Generating Daily Data'
		open('list_previous_day.txt', 'w').close()
		getter = Google_Quotes()
		getter.setParameters(60,1) #1 Day
		getter.getCompanies(self.symboles)
		getter.fetchCompanyDataPre()

		num_lines = sum(1 for line in open('list_previous_day.txt', 'r'))
		self.nb_companies=num_lines
			
		end_time = time.time()
		print(" (%5.2f s.)" % (end_time - start_time))

	def read_new_list_companies(self):
		f = open('list_previous_day.txt', 'r')
		i=0
		self.symboles=["TEST"]
		for line in f:
			if(i==0):
				text=line.split(',')
				self.symboles[i]=text[0]
			else:
				text=line.split(',')
				self.symboles.append(text[0])
			i=i+1
		f.close()
		self.nb_companies=i
		print ("%d companies will be traded today." % (i))

	def get_HighLow20_limit(self):
		start_time = time.time()
		print 'Calculating 20 Minutes Limits'
		#Get High Low from Google for the first 20 minutes
		getter = Google_Quotes()
		getter.setParameters(1200,1)
		getter.getCompanies(self.symboles)
		getter.fetchCompanyData20()
		var=getter.get_limits()

		#Calculate the high and low
		for symb in self.symboles:
			self.company_limits[symb,'high20']=var[symb,'high20']
			self.company_limits[symb,'low20']=var[symb,'low20']

		end_time = time.time()
		print(" (%4.2f s.)" % (end_time - start_time))

		#check length
		if len(self.company_limits)==self.nb_companies*4:
			print 'Success'
		else:
			print("Failure (%d/%d)" % (len(self.company_limits),self.nb_companies*4))

	def return_limits(self):
		return self.company_limits

	#Find Firms which respect the limits
	def find_good_firms(self):
		for symb in self.symboles:
			if self.volatility[symb]>=0.3:
				if self.company_limits[symb,'high20'] > self.company_limits[symb,'lowPre'] and \
				self.company_limits[symb,'high20'] < self.company_limits[symb,'highPre']:
					if (self.company_limits[symb,'highPre'] - self.company_limits[symb,'high20']) > 0.1 and \
					self.company_limits[symb,'low20'] < self.company_limits[symb,'lowPre']:
						self.firms_symb[symb]=1 #HIGH
				elif self.company_limits[symb,'low20'] > self.company_limits[symb,'highPre'] and \
				self.company_limits[symb,'low20'] < self.company_limits[symb,'lowPre']:
					if (self.company_limits[symb,'low20'] - self.company_limits[symb,'lowPre']) > 0.1 and \
					self.company_limits[symb,'high20'] < self.company_limits[symb,'highPre']:
						self.firms_symb[symb]=2 #LOW

		for symb, val in self.firms_symb.items():
			if (val==1):
				self.firms_trade[symb,'low20','high']=self.company_limits[symb,'low20']
				self.firms_trade[symb,'high20','high']=self.company_limits[symb,'high20']
				self.firms_trade[symb,'lowPre','high']=self.company_limits[symb,'lowPre']
				self.firms_trade[symb,'highPre','high']=self.company_limits[symb,'highPre']
			else:
				self.firms_trade[symb,'low20','low']=self.company_limits[symb,'low20']
				self.firms_trade[symb,'high20','low']=self.company_limits[symb,'high20']
				self.firms_trade[symb,'lowPre','low']=self.company_limits[symb,'lowPre']
				self.firms_trade[symb,'highPre','low']=self.company_limits[symb,'highPre']

		print ("%d companies will be traded today." % (len(initAlgo.firms_trade)))


	def launch(self):
		self.read_list_companies()
		#self.symboles=['A','AA','AAN','AAP','AAPL','AAXJ','AB','ABB','ABC','ABG']
		#self.generate_daily_data()
		self.read_new_list_companies()
		self.get_HighLowPre_limit()
		#Wait for 15:50...
		self.get_HighLow20_limit()
		self.find_good_firms()


initAlgo=InitDarkKnight()
initAlgo.launch()

#Get Daily data from Google and save in file --> put this at the end of Dark Knight

# #Generate Contract List only for good companys
# contractDict = filesHandling.generate_contracts(company_symb)

