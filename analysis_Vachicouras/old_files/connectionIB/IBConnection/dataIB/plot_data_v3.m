%Plot Parsed Data
clear;
for n=1:3
    
    name={'AAPL_18_11_2013.txt', 'GOOG_18_11_2013.txt','IBM_18_11_2013.txt'};
    data_ib=importfile(char(name(n)));
    
    name={'AAPL.csv', 'GOOG.csv','IBM.csv'};
    data_goog=importfile1(char(name(n)));
    %column 6 is close data, column 3 is open

    for i=1:length(data_goog)
        data_goog(i,1)=(i-1)*60;
    end
    
    data_goog_low=zeros(length(data_goog)*60,2);
    data_goog_high=zeros(length(data_goog)*60,2);
    %expand google data
    for i=1:(length(data_goog))*60
        data_goog_low(i,1)=i-60;
        data_goog_low(i,2)=data_goog(ceil(i/60),5);
        data_goog_high(i,1)=i-60;
        data_goog_high(i,2)=data_goog(ceil(i/60),4);
    end
    
    h=figure('units','normalized','outerposition',[0 0 1 1], 'Visible','on');
    hold on;
    plot(data_ib(:,1),data_ib(:,3),'k');
    hold on;
    plot(data_goog_low(:,1),data_goog_low(:,2),'r');
    hold on;
    plot(data_goog_high(:,1),data_goog_high(:,2),'b');
    hold on;
    xlim([17880 20640]);
    title_str=[char(name(n))];
    title(title_str,'FontSize',18,'FontName','Helvetica');
    name2=[title_str,'_image.png'];
    set(h, 'PaperPositionMode','auto');   %# WYSIWYG
    print('-dpng','-r0',name2);
end