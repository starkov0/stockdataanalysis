%Plot Parsed Data
clear;
for i=1:1
    
    name={'AAPL_18_11_2013.txt', 'GOOG_18_11_2013.txt'};
    data2=importfile(char(name(i)));
    
    name={};

    zero_day=datenum(2013,11,18,15,30,0);

    %Calculate new date
    %[A,ind]=unique(data2(:,1));
    %data=data2(ind,:);
    data=data2;
    hours=floor(data(:,1)/3600);
    minutes=floor((data(:,1)-(hours*3600))/60);
    seconds=floor(data(:,1)-hours*3600-minutes*60);
    date2=datestr(datenum(2013,11,18,15+hours-6,30+minutes,0+seconds),'HH:MM:SS');
    date=cellstr(date2);

    
    google_t=[];
    google_p=[];
    figure;
    tick=[1:100:length(date)];
    plot(data(:,3));
    set(gca,'XTick',tick,'XTickLabel',date(tick));
end

