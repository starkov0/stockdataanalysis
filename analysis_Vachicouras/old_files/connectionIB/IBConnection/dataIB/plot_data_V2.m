%Plot Parsed Data
clear;
for i=1:1
    
    name={'AAPL_18_11_2013.txt', 'GOOG_18_11_2013.txt'};
    data=importfile(char(name(i)));
    
    name={'AAPL.csv', 'GOOG.csv'};
    data_goog=importfile1(char(name(i)));
    %column 6 is close data

    hours=floor(data(:,1)/3600);
    minutes=floor((data(:,1)-(hours*3600))/60);
    seconds=floor(data(:,1)-hours*3600-minutes*60);
    date2=datestr(datenum(2013,11,18,15+hours-6,30+minutes,0+seconds),'HH:MM:SS');
    date=cellstr(date2);

    for i=1:length(data_goog)
        date_goog(i)=datenum(2013,11,18,14-6,27,59+i);
    end
    
    google_t=[];
    google_p=[];
    
    figure;
    tick=[1:100:length(date)];
    disp(date(1));
    plot(data(:,3));
    hold on;
    set(gca,'XTick',tick,'XTickLabel',date(tick));
end

