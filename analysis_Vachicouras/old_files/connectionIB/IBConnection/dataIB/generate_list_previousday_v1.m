%Plot Parsed Data
clear;

N=3;
list_data=zeros(N,4);
fileID = fopen('list_previous_day.txt','w');
%SYMB PREV_HIGH PREV_LOW PREV_CLOSE
for n=1:N
    
    name={'AAPL_18_11_2013.txt', 'GOOG_18_11_2013.txt','IBM_18_11_2013.txt'};
    data_ib=importfile(char(name(n)));
    symb=strsplit(char(name(n)),'_');
    symb=symb(1);
    
    fprintf(fileID,'%s %.3f %.3f %.3f\n',char(symb),max(data_ib(:,3)),min(data_ib(:,3)),data_ib(end,3));
    
end
fclose(fileID);
