#! /usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
from ib.ext.Contract import Contract
from ib.opt import ibConnection, message
from time import sleep
from parseData import ParseData

####################################################################################################
parser = ParseData()
# print all messages from TWS
def watcher(msg):
    print msg

# show Bid and Ask quotes
def my_BidAsk(msg):
    time = datetime.now()
    if msg.field == 1:
        parser.pushData(contractDict[msg.tickerId][0], "bid", msg.price, time)
    elif msg.field == 2:
        parser.pushData(contractDict[msg.tickerId][0], "ask", msg.price, time)

def makeStkContract(contractTuple):
    newContract = Contract()
    newContract.m_symbol = contractTuple[0]
    newContract.m_secType = contractTuple[1]
    newContract.m_exchange = contractTuple[2]
    newContract.m_currency = contractTuple[3]
    newContract.m_expiry = contractTuple[4]
    newContract.m_strike = contractTuple[5]
    newContract.m_right = contractTuple[6]
    return newContract

####################################################################################################

contractDict = {}
contractDict[1] = ('AAPL', 'STK', 'SMART', 'USD', '', 0.0, '')
contractDict[2] = ('SPY', 'STK', 'SMART', 'USD', '', 0.0, '')
contractDict[3] = ('IBM', 'STK', 'SMART', 'USD', '', 0.0, '')
contractDict[4] = ('GOOG', 'STK', 'SMART', 'USD', '', 0.0, '')
contractDict[5] = ('ACO', 'STK', 'SMART', 'USD', '', 0.0, '')
contractDict[6] = ('ABG', 'STK', 'SMART', 'USD', '', 0.0, '')

####################################################################################################

if __name__ == '__main__':
    con = ibConnection()
    con.registerAll(watcher)
    showBidAskOnly = True  # set False to see the raw messages
    if showBidAskOnly:
        con.unregister(watcher, message.TickSize, message.TickPrice,
                       message.TickString, message.TickOptionComputation)
        con.register(my_BidAsk, message.TickPrice)
    con.connect()
    sleep(1)
    print '* * * * REQUESTING MARKET DATA * * * *'
    for tickId in range(1,7):
        stkContract = makeStkContract(contractDict[tickId])
        con.reqMktData(tickId, stkContract, '', False)
    sleep(6300)
    print '* * * * CANCELING MARKET DATA * * * *'
    for tickId in range(1,7)
        con.cancelMktData(tickId)
    sleep(1)
    con.disconnect()
    sleep(1)
