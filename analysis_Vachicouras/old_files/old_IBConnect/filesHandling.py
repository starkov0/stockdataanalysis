from Variables import Variables

# python unlink files that you dont need
import glob

class filesHandling(object):
	
	def __init__(self):
		super(filesHandling,self).__init__()
		self.varObj = Variables()
	
	def generate_contracts(self,company_symb):
		index=1
		contractDict = {}
		for symb in company_symb:
			contractDict[index] = (symb, 'STK', 'SMART', 'USD', '', 0.0, '')
			index=index+1
		return contractDict
