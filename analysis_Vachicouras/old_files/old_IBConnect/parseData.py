from datetime import datetime
import csv

class ParseData(object):
    
    def __init__(self):
        super(ParseData, self).__init__()
        currentTime = datetime.now()
        self.start = datetime(currentTime.year, currentTime.month, currentTime.day, 15, 30, 00)
        
    def writeCSV(self,name,data):
        f = open(name,'a')
        wr = csv.writer(f, dialect='excel')
        wr.writerow(data)
        f.close()            
    
    def pushData(self,companyName,askBid,price,time):
        theTime = time - self.start
        message = [theTime.seconds, theTime.microseconds, price, askBid]
        name = "./dataIB/"+companyName+"_"+str(self.start.day)+"_"+str(self.start.month)+"_"+str(self.start.year)+".txt"
        self.writeCSV(name,message)